# Cloning

* [Source Code](#Source-Code)
* [Repositories](#Repositories)
* [Packages](#Packages)
* [Facilitated By](#Facilitated-By)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [logging] - ISR framework logging libraries
* [tracing] - ISR framework tracing libraries
* [json] - ISR framework libraries for .NET Standard
```
git clone git@bitbucket.org:davidhary/dn.json.git
git clone git@bitbucket.org:davidhary/dn.logging.git
git clone git@bitbucket.org:davidhary/dn.tracing.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%vslib%\core\logging
%vslib%\core\tracing
%vslib%\core\json
```
where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions

### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[logging]: https://www.bitbucket.org/davidhary/dn.logging
[tracing]: https://www.bitbucket.org/davidhary/dn.tracing
[json]: https://www.bitbucket.org/davidhary/dn.json
[Exception Extension]: https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
[Serilog]: https://github.com/serilog/serilog
[Serilog.Enrichers.Demystify]: https://github.com/nblumhardt/serilog-enrichers-demystify
[Serilog.Sinks.Async]: https://github.com/serilog/serilog-sinks-async 
[Serilog.Sinks.File]: https://github.com/serilog/serilog-sinks-file
[Serilog.Sinks.File.Header]: https://github.com/cocowalla/serilog-sinks-file-header

[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[external repositories]: ExternalReposCommits.csv

