# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2.0.9083] - 2024-11-13 Preview 202304
* Update packages.
* Upgrade Forms libraries and Test and Demo apps to .Net 9.0.
* Increment version to 9083.

## [2.0.8977] - 2024-07-30 Preview 202304
* Add category to the Trace Logger and remove the singleton instance;
* Add a Tracer class with a Tracer category;
* Add Trace Logger extensions;
* Move trace event and log level key value pairs to selector classes;
* Move opening the trace log file to a Trace Log Explorer class;
* Create a new OrLog logger which now supports the .NET Frameworks (a package reference to Serilog.Settings.Configuration is required to ensure that the correct version of Serilog is loaded.)

## [1.1.8956] - 2024-07-09 Preview 202304
* Set projects to target .NET Standard 2.0 alone.
* Update .NET standard 2.0 enhanced methods and attributes.

## [1.1.8949] - 2024-07-02 Preview 202304
* Update to .Net 8.
* Implement MS Test SDK project format.
* Update libraries versions.
* Use ordinal instead of ordinal ignore case string comparisons.
* Apply constants style.
* Apply code analysis rules.
* Generate assembly attributes.
* Remove redundant assembly info of Serilog projects.

## [1.1.8536] - 2023-05-15 Preview 202304
* Use cc.isr.Json.AppSettings.ViewModels project for settings I/O.
* Use singlton instance of the test site settings class.

## [1.1.8526] - 2023-05-06 Preview 202304
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.
* Increment version.
* App Settings: Use Lazy singleton instance. Correct the construction of the default settings file name.
* test: initialize the settings file name to override the `testhost` file name.

## [1.0.8505] - 2023-03-25
* Prefix namespaces with 'cc'. Implement nullable. Add creative commons license.

## [1.0.8484] - 2023-03-25
* Add isr.Logging.LogWriter project to the logging solution. 

## [1.0.8125] - 2022-03-31
* Pass tests in project reference mode. 

## [1.0.8125] - 2022-03-31
* Pass tests in project reference mode. 

## [1.0.8119] - 2022-03-25
* Add windows forms controls.

## [1.0.8118] - 2022-03-24
* MS Test: use update json settings.

## [1.0.8110] - 2022-03-16
* Use the ?. operator, without making a copy of the delegate, 
to check if a delegate is non-null and invoke it in a thread-safe way.

## [1.0.8103] - 2022-03-09
* Forked from [dn.core].

&copy; 2012 Integrated Scientific Resources, Inc. All rights reserved.

[dn.core]: https://www.bitbucket.org/davidhary/dn.core
[2.0.9083]: https://bitbucket.org/davidhary/dn.logging
