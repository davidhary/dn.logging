# About

cc.isr.Serilog.Enrichers.Demystify is a .Net library
fork of Serilog Enricher Demistify further enriching the exception output.

# How to Use
Requires no changes in the Serilog configuration file.

# Key Features

* Enriches the exception output

# Main Types

The main types provided by this library are:

* _ExceptionExtensions.Demistify<T>_ Demystifies the given exception
  and tracks the original stack traces for the whole exception tree.

# Feedback

cc.isr.Serilog.Enrichers.Demystify is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Logging Repository].

[Logging Repository]: https://bitbucket.org/davidhary/dn.logging

