// Copyright (c) Ben A Adams. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System.Reflection;
using System.Text;

namespace System.Diagnostics;

/// <summary>   An exception extensions. </summary>
/// <remarks>   David, 2021-02-18. </remarks>
public static class ExceptionExtensions
{
    [CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
    private static readonly FieldInfo? stackTraceString = typeof( Exception ).GetField( "_stackTraceString", BindingFlags.Instance | BindingFlags.NonPublic );

    private static void SetStackTracesString( this Exception exception, string value )
    {
        stackTraceString?.SetValue( exception, value );
    }

    /// <summary>   Builds exception information. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="exception">    The exception. </param>
    /// <returns>   The given data converted to a <see cref="string" />. </returns>
    private static string BuildExceptionInfo( Exception exception )
    {
        StringBuilder builder = new();
        string stackTrace = exception.StackTrace;
        if ( !string.IsNullOrEmpty( stackTrace ) )
            _ = builder.AppendLine( stackTrace );
        int counter = 1;
        _ = AppendExceptionInfo( builder, exception, counter );
        return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
    }

    /// <summary>   Appends an exception information. </summary>
    /// <remarks>   David, 2021-02-17. </remarks>
    /// <param name="builder">      The builder. </param>
    /// <param name="exception">    The exception. </param>
    /// <param name="counter">      The counter. </param>
    /// <returns>   An int. </returns>
    private static int AppendExceptionInfo( StringBuilder builder, Exception exception, int counter )
    {
        AppendExceptionInfo( builder, exception, $"{counter}->" );
        counter += 1;
        if ( exception is AggregateException aggEx )
        {
            foreach ( Exception? ex in aggEx.InnerExceptions )
            {
                counter = AppendExceptionInfo( builder, exception, counter );
            }
        }
        if ( exception.InnerException is not null )
            counter = AppendExceptionInfo( builder, exception.InnerException, counter );

        return counter;
    }

    /// <summary>   Appends an exception information. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="builder">      The builder. </param>
    /// <param name="exception">    The exception. </param>
    /// <param name="prefix">       The prefix. </param>
    private static void AppendExceptionInfo( StringBuilder builder, Exception exception, string prefix )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( exception, nameof( exception ) );
#else
        if ( exception is null ) throw new ArgumentNullException( nameof( exception ) );
        if ( builder is null ) throw new ArgumentNullException( nameof( builder ) );
#endif

        const int WIDTH = 8;

        _ = builder.AppendLine( $"{prefix}{nameof( System.Type ),WIDTH}: {exception.GetType()}" );
        if ( !string.IsNullOrEmpty( exception.Message ) )
        {
            _ = builder.AppendLine( $"{prefix}{nameof( Exception.Message ),WIDTH}: {exception.Message}" );
        }

        if ( !string.IsNullOrEmpty( exception.Source ) )
        {
            _ = builder.AppendLine( $"{prefix}{nameof( Exception.Source ),WIDTH}: {exception.Source}" );
        }

        if ( exception.TargetSite is not null )
        {
            _ = builder.AppendLine( $"{prefix}  Method: {exception.TargetSite}" );
        }

        if ( exception.HResult != 0 )
        {
            _ = builder.AppendLine( $"{prefix}{nameof( Exception.HResult ),WIDTH}: {exception.HResult} ({exception.HResult:X})" );
        }

        if ( exception.Data is object )
        {
            foreach ( Collections.DictionaryEntry keyValuePair in exception.Data )
            {
                _ = builder.AppendLine( $"{prefix}    Data: {keyValuePair.Key}: {keyValuePair.Value}" );
            }
        }
    }

    /// <summary>
    /// Demystifies the given <paramref name="exception"/> and tracks the original stack traces for the whole exception tree.
    /// </summary>
    public static T Demystify<T>( this T exception ) where T : Exception
    {
        try
        {
            string info = BuildExceptionInfo( exception );
            exception.SetStackTracesString( info );
#if false
            var stackTrace = new EnhancedStackTrace(exception);

            if (stackTrace.FrameCount > 0)
            {
                StringBuilder builder = new StringBuilder( stackTrace.ToString() );
                foreach ( System.Collections.DictionaryEntry kvp in exception.Data )
                {
                    _ = builder.AppendLine( $"{kvp.Key}:: {kvp.Value}" );
                }
                exception.SetStackTracesString( builder.ToString());
            }

            if (exception is AggregateException aggEx)
            {
                foreach (var ex in EnumerableIList.Create(aggEx.InnerExceptions))
                {
                    ex.Demystify();
                }
            }

            exception.InnerException?.Demystify();
#endif
        }
        catch
        {
            // Processing exceptions shouldn't throw exceptions; if it fails
        }

        return exception;
    }

#if false
    /// <summary>
    /// Gets demystified string representation of the <paramref name="exception"/>.
    /// </summary>
    /// <remarks>
    /// <see cref="Demystify{T}"/> method mutates the exception instance that can cause
    /// issues if a system relies on the stack trace be in the specific form.
    /// Unlike <see cref="Demystify{T}"/> this method is pure. It calls <see cref="Demystify{T}"/> first,
    /// computes a demystified string representation and then restores the original state of the exception back.
    /// </remarks>
    [Contracts.Pure]
    public static string ToStringDemystified(this Exception exception) 
        => new StringBuilder().AppendDemystified(exception).ToString();
#endif
}
