using System.Diagnostics;
using Serilog.Core;
using Serilog.Events;

namespace Serilog.Enrichers.Demystify;

/// <summary>   A demystified stack trace enricher. </summary>
/// <remarks>   David, 2021-07-03. </remarks>
internal class DemystifiedStackTraceEnricher : ILogEventEnricher
{
    public void Enrich( LogEvent logEvent, ILogEventPropertyFactory propertyFactory )
    {
        _ = (logEvent.Exception?.Demystify());
    }
}
