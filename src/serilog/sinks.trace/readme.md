# About

cc.isr.Serilog.Sinks.Trace is a .Net library 
Fork of Serilog Sinks Trace removing headers from the trace output.

# How to Use
This code example is included in the source code and the 
Serilog Tester demo of the [Logging Repository]

Add a hook to the Serilog Json configuration file:
```
"configure": [
{
    "Name": "File",
    "Args": {
    "hooks": "Serilog.Demo.SerilogHooks::MyHeaderWriter, Serilog.Demo",
    "path": "../_logs/ex_.log",
    "outputTemplate": "{Timestamp:HH:mm:ss.fff zzz}, [{Level:u3}], ({SourceContext}), {Message}{NewLine}{Exception}",
    "rollingInterval": "Day",
    "retainedFileCountLimit": 7
    }
}
]
```

```
namespace Serilog.Sinks.File.Header
{

    /// <inheritdoc />
    /// <summary>
    /// Writes a header at the start of every log file
    /// </summary>
    /// <remarks> Forked from https://github.com/cocowalla/serilog-sinks-file-header <para>
    /// Adds getting the file name.
    /// </para>
    /// </remarks>
    public class HeaderWriter : FileLifecycleHooks
    {
        // Same as the default StreamWriter buffer size
        private const int DEFAULT_BUFFER_SIZE = 1024;

        // Factory method to generate the file header
        private readonly Func<string> _HeaderFactory;

        // Whether to always write the header, even for non-empty files
        private readonly bool _AlwaysWriteHeader;

        public HeaderWriter( string header, bool alwaysWriteHeader = false )
        {
            this._HeaderFactory = () => header;
            this._AlwaysWriteHeader = alwaysWriteHeader;
        }

        public HeaderWriter( Func<string> headerFactory, bool alwaysWriteHeader = false )
        {
            this._HeaderFactory = headerFactory;
            this._AlwaysWriteHeader = alwaysWriteHeader;
        }

        /// <summary>   Gets or sets the filename of the full file. </summary>
        /// <value> The filename of the full file. </value>
        public static string FullFileName { get; private set; }

        /// <summary>   Waits for the first setup of the <see cref="FullFileName"/>. </summary>
        /// <remarks>   David, 2021-02-09. </remarks>
        /// <param name="timeout">  The timeout. </param>
        /// <param name="dueTime">  The due time. </param>
        /// <param name="period">   The period. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool AwaitFullFileName( TimeSpan timeout, TimeSpan dueTime, TimeSpan period )
        {
            // Create an AutoResetEvent to signal the timeout threshold in the
            // timer callback has been reached.
            var autoEvent = new AutoResetEvent( false );

            var fullFileNameChecker = new FullFileNameChecker( timeout );

            // Create a timer that invokes the file open checked after 'due time' and every 'period' time thereafter.
            var stateTimer = new Timer( fullFileNameChecker.CheckFullFileName, autoEvent, dueTime, period );

            // When autoEvent signals, dispose of the timer.
            _ = autoEvent.WaitOne();
            stateTimer.Dispose();

            return !string.IsNullOrEmpty( HeaderWriter.FullFileName );
        }

        private class FullFileNameChecker
        {

            private TimeSpan _Timeout;
            private readonly Stopwatch _Sw;

            public FullFileNameChecker( TimeSpan timeout )
            {
                this._Timeout = timeout;
                this._Sw = Stopwatch.StartNew();
            }

            /// <summary>   Restarts the given timeout. </summary>
            /// <remarks>   David, 2021-02-09. </remarks>
            /// <param name="timeout">  The timeout. </param>
            public void Restart( TimeSpan timeout )
            {
                this._Timeout = timeout;
                this._Sw.Restart();
            }

            /// <summary>   Queries if the full file name was set. </summary>
            /// <remarks>   David, 2021-02-09.  This method is called by the timer delegate.
            /// </remarks>
            /// <param name="stateInfo">    Information describing the state. </param>
            public void CheckFullFileName( Object stateInfo )
            {
                AutoResetEvent autoEvent = ( AutoResetEvent ) stateInfo;

                if ( !string.IsNullOrEmpty( HeaderWriter.FullFileName ) || (this._Sw.Elapsed > this._Timeout) )
                {
                    // signal the waiting thread.
                    _ = autoEvent.Set();
                }
            }
        }

        /// <summary>
        /// Initialize or wrap the <paramref name="underlyingStream" /> opened on the log file. This can
        /// be used to write file headers, or wrap the stream in another that adds buffering, compression,
        /// encryption, etc. The underlying file may or may not be empty when this method is called.
        /// </summary>
        /// <remarks>
        /// A value must be returned from overrides of this method. Serilog will flush and/or dispose the
        /// returned value, but will not dispose the stream initially passed in unless it is itself
        /// returned.
        /// </remarks>
        /// <param name="path">             The full path to the log file. </param>
        /// <param name="underlyingStream"> The underlying <see cref="T:System.IO.Stream" /> opened on
        ///                                 the log file. </param>
        /// <param name="encoding">         The encoding to use when reading/writing to the stream. </param>
        /// <returns>
        /// The <see cref="T:System.IO.Stream" /> Serilog should use when writing events to the log file.
        /// </returns>
        public override Stream OnFileOpened( string path, Stream underlyingStream, Encoding encoding )
        {

            try
            {
                HeaderWriter.FullFileName = path;
                if ( this._AlwaysWriteHeader && underlyingStream.Length != 0 )
                {
                    SelfLog.WriteLine( $"File header will not be written, as the stream already contains {underlyingStream.Length} bytes of content" );
                    return base.OnFileOpened( underlyingStream, encoding );
                }
            }
            catch ( NotSupportedException )
            {
                // Not all streams support reading the length - in this case, we always write the header,
                // otherwise we'd *never* write it!
            }

            using ( var writer = new StreamWriter( underlyingStream, encoding, _DefaultBufferSize, true ) )
            {
                var header = this._HeaderFactory();

                writer.WriteLine( header );
                writer.Flush();
                underlyingStream.Flush();
            }

            return base.OnFileOpened( underlyingStream, encoding );
        }
    }
}
```

This code is located in the Logging package of the [Logging Repository].

```
using System;
using System.Linq;
using System.Reflection;

using Serilog.Sinks.File.Header;

namespace cc.isr.Logging
{
    /// <summary>   A Serilog hooks. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    public class SerilogHooks
    {

        /// <summary>   Builds time caption using <see cref="DateTimeOffset"/> showing local time and offset. </summary>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        /// <param name="timeCaptionFormat">    The time caption format. </param>
        /// <param name="kindFormat">           The kind format. </param>
        /// <returns>   A string. </returns>
        public static string BuildLocalTimeCaption( string timeCaptionFormat, string kindFormat )
        {
            string result = string.IsNullOrWhiteSpace( timeCaptionFormat )
                ? $"{DateTimeOffset.Now}"
                : string.IsNullOrWhiteSpace( kindFormat )
                    ? $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}"
                    : $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}{DateTimeOffset.Now.ToString( kindFormat )}";
            return result;
        }

        /// <summary> Prefix process name to the product name. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> A String. </returns>
        public static string PrefixProcessName( Assembly assemblyInfo )
        {
            if ( assemblyInfo is null )
            {
                throw new ArgumentNullException( nameof( assemblyInfo ) );
            }

            var productName = assemblyInfo
                .GetCustomAttributes( typeof( AssemblyProductAttribute ) )
                .OfType<AssemblyProductAttribute>()
                .FirstOrDefault().Product;
            string processName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
            if ( !productName.StartsWith( processName, StringComparison.Ordinal ) )
            {
                productName = $"{processName}.{productName}";
            }
            return productName;
        }

        /// <summary> Builds product time caption. </summary>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo">      Information describing the assembly. </param>
        /// <param name="versionElements">   The version elements. </param>
        /// <param name="timeCaptionFormat"> The time caption format. </param>
        /// <param name="kindFormat">        The kind format. </param>
        /// <returns> A String. </returns>
        public static string BuildProductTimeCaption( Assembly assemblyInfo, int versionElements, string timeCaptionFormat, string kindFormat )
        {
            return assemblyInfo is null
                ? throw new ArgumentNullException( nameof( assemblyInfo ) )
                : $"{PrefixProcessName( assemblyInfo )}.r.{assemblyInfo.GetName().Version.ToString( versionElements )} {SerilogHooks.BuildLocalTimeCaption( timeCaptionFormat, kindFormat )}";
        }

        /// <summary>   Gets file name. </summary>
        /// <remarks>   David, 2021-02-06. </remarks>
        /// <param name="stream"> The stream. </param>
        /// <returns>   The file name.
        ///             see this for changing to get the return value of the file name,
        ///             https://github.com/cocowalla/serilog-sinks-file-header/blob/master/src/Serilog.Sinks.File.Header/HeaderWriter.cs </returns>
        public static string GetFileName( System.IO.Stream stream )
        {
            return stream is System.IO.FileStream fs
                ? fs.Name
                : string.Empty;
        }

        /// <summary>
        /// Builds product time caption using full version and local time plus kind format.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> A String. </returns>
        public static string BuildProductTimeCaption( Assembly assemblyInfo )
        {
            return BuildProductTimeCaption( assemblyInfo, 4, string.Empty, string.Empty );
        }

        /// <summary>
        /// Builds product time caption using full version and local time plus kind format.
        /// </summary>
        /// <remarks>   David, 2021-02-08. </remarks>
        /// <returns>   A String. </returns>
        public static string BuildProductTimeCaption()
        {
            // the entry assembly might be null when called from the test platform, 
            // because the entry assembly must be a managed type, which the test platform might not.
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetEntryAssembly() is null
                                                        ? System.Reflection.Assembly.GetCallingAssembly()
                                                        : System.Reflection.Assembly.GetEntryAssembly();
            return BuildProductTimeCaption( assembly, 4, string.Empty, string.Empty );
        }

        /// <summary>   Gets my header writer. </summary>
        /// <value> my header writer. </value>
        public static HeaderWriter MyHeaderWriter => new( @$"{BuildProductTimeCaption()}{Environment.NewLine}Timestamp, Level, Source, Message" );
    }

}
```

# Key Features

* Removes headers from the trace output.

# Main Types

The main types provided by this library are:
* _TraceSync_ Fork of the trace sync.

# Feedback

cc.isr.Serilog.Sinks.Trace is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Logging Repository].

[Logging Repository]: https://bitbucket.org/davidhary/dn.logging

