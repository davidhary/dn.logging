using System;
using Microsoft.Extensions.Logging;

namespace Serilog.Demo.Extensions;

/// <summary>   A logger extensions. </summary>
/// <remarks>   2024-07-26. </remarks>
internal static partial class ILoggerExtensions
{
    /// <summary>
    /// Writes a Information message.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="logger">           The logger. </param>
    /// <param name="message">          The message. </param>
    [LoggerMessage( 20, LogLevel.Information, "{message}", EventName = "LogInformationMessage" )]
    public static partial void LogInformationMessage( this Microsoft.Extensions.Logging.ILogger logger, string message );

    /// <summary>   Writes a Exception message. </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="logger">       The logger. </param>
    /// <param name="message">      The message. </param>
    /// <param name="exception">    The exception. </param>
    [LoggerMessage( 43, LogLevel.Error, "{message}", EventName = "LogExceptionMessage" )]
    public static partial void LogExceptionMessage( this Microsoft.Extensions.Logging.ILogger logger, string message, Exception exception );

    /// <summary>
    /// Writes a Debug message.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="logger">           The logger. </param>
    /// <param name="message">          The message. </param>
    [LoggerMessage( 10, LogLevel.Debug, "{message}", EventName = "LogDebugMessage" )]
    public static partial void LogDebugMessage( this Microsoft.Extensions.Logging.ILogger logger, string message );

}
