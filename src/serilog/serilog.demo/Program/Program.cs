using System;
using System.Linq;
using System.CommandLine;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog.Debugging;
using Serilog.Sinks.File.Header;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Reflection;
using System.Runtime.Versioning;
using Serilog.Demo.Extensions;

namespace Serilog.Demo;

/// <summary>   A program. </summary>
/// <remarks>   David, 2021-02-04. </remarks>
internal sealed class Program
{
    /// <summary>   Main entry-point for this application. </summary>
    /// <remarks>   2023-04-18. </remarks>
    /// <param name="args"> An array of command-line argument strings. </param>
    /// <returns>   Exit-code for the process - 0 for success, else an error code. </returns>
    private static async Task<int> Main( string[] args )
    {
        RootCommand rootCommand = Program.CreateRootCommand();
        Program.DefineCommandLIneOptions( rootCommand );
        Program.DefineExecution( rootCommand );
        return await rootCommand.InvokeAsync( args )
            .ContinueWith( failedTask => OnThreadException( new ThreadExceptionEventArgs( failedTask.Exception ) ) ); ;
    }

    private static int OnThreadException( ThreadExceptionEventArgs args )
    {
        if ( args == null || args.Exception is null ) return 0;
        Console.WriteLine( $"Exception; {Environment.NewLine}{args.Exception}." );
        return -1;
    }

    /// <summary>   Define execution. </summary>
    /// <remarks>   2023-04-18. </remarks>
    /// <param name="rootCommand">  The root command. </param>
    private static void DefineExecution( RootCommand rootCommand )
    {
        Option<string> basicOption = ( Option<string> ) rootCommand.Options.FirstOrDefault( x => x.Name == "option" );
        if ( basicOption is not null )
        {
            rootCommand.SetHandler( ( option ) =>
            {
                Console.WriteLine( $"Running the '{option}' option..." );
                switch ( option.ToUpperInvariant() )
                {
                    case "SERILOG":
                        RunSerilogImplementation();
                        break;

                    default:
                        break;
                }
                Console.Write( "\r\n\r\nDone. Enter key: " );
                _ = Console.ReadKey();
            },
            basicOption );
        }
        else
        {
            string helpText = rootCommand.Options.FirstOrDefault( x => x.Name == "help" ).Description;
            Console.WriteLine( $"Logger option not specified; {Environment.NewLine}{helpText}" );
        }
    }

    #region " command line parsing "

    /// <summary>   Creates the root command. </summary>
    /// <remarks>   2023-04-18. </remarks>
    /// <returns>   The new root command. </returns>
    private static RootCommand CreateRootCommand()
    {
        // Instantiate the command line application
        RootCommand app = new()
        {
            // This should be the name of the executable itself. the help text line "Usage: ConsoleArgs" uses this
            Name = "LoggingTester",
            Description = ".NET Core logging tester",
            // ExtendedHelpText = @"This program demonstrates the logging functionality of the cc.isr.Logging.Logger."
        };
        return app;
    }

    /// <summary>   Define command line options. </summary>
    private static void DefineCommandLIneOptions( RootCommand rootCommand )
    {
        // Arguments: -o listener -l error -d ..\_log -f Column

        // The first argument is the option template.
        // It starts with a pipe-delimited list of option flags/names to use
        // Optionally, It is then followed by a space and a short description of the value to specify.
        // e.g. here we could also just use "-o|--option"
        rootCommand.AddOption( new Option<string>( ["-o", "--option"], "Listener or Provider Logging implementation" ) );
        rootCommand.AddOption( new Option<LogLevel>( ["-l", "--level"], "Logging level: Information, Warning, Error" ) );
        rootCommand.AddOption( new Option<DirectoryInfo>( ["-d", "--dir"], "Relative Directory, e.g., ..\\_log" ) );
        rootCommand.AddOption( new Option<string>( ["-f", "--form"], "Logging format: Column, Row, Tab, Comma" ) );
    }

    #endregion

    #region " serilog implementation "

    private static void ConfigureSerilogServices( string settingsFileName, IServiceCollection serviceCollection )
    {
        // Add logging
        _ = serviceCollection.AddLogging( loggingBuilder =>
        {
            _ = loggingBuilder.AddSerilog();
            _ = loggingBuilder.AddConsole();
            _ = loggingBuilder.AddDebug();
        } );

        // Build configuration
        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath( AppContext.BaseDirectory )
            .AddJsonFile( settingsFileName, false )
            .Build();

        // Initialize the Serilog logger using Json configuration file.
        Serilog.Log.Logger = new LoggerConfiguration()
             .ReadFrom.Configuration( configuration )
             .CreateLogger();

        // Add access to generic IConfigurationRoot
        _ = serviceCollection.AddSingleton( configuration );

        // Add services
        _ = serviceCollection.AddTransient<ITestService, TestService>();

        // Add the application
        _ = serviceCollection.AddTransient<App>();
    }

    /// <summary>
    /// Builds application settings file title and extension consisting of the
    /// <paramref name="assemblyTitle"/>+<paramref name="suffix"/>+.json.
    /// </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="assemblyTitle">    The assembly file title. </param>
    /// <param name="suffix">           (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A file title and extension. </returns>
    public static string BuildAppSettingsFileName( string assemblyTitle, string suffix = ".Serilog" )
    {
        return $"{assemblyTitle}{suffix}.json";
    }

    /// <summary>
    /// Builds application settings file name consisting of the
    /// 'Calling Assembly Name'+<paramref name="suffix"/>+.json.
    /// </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="suffix">   (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildAppSettingsFileName( string suffix = ".Serilog" )
    {
        return Program.BuildAppSettingsFileName( System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, suffix );
    }

    /// <summary>   Executes the 'provider implementation' operation. </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    private static void RunSerilogImplementation()
    {
        // Create service collection
        ServiceCollection serviceCollection = new();
        ConfigureSerilogServices( Program.BuildAppSettingsFileName(), serviceCollection );

        // Create service provider
        ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

        // a logger can be instantiated at this point. 
        ILogger<Program> logger = serviceProvider.GetService<ILoggerFactory>()
                                   .CreateLogger<Program>();

        SelfLog.Enable( Console.Error );

        TargetFrameworkAttribute framework = Assembly.GetEntryAssembly().GetCustomAttribute<TargetFrameworkAttribute>();
        System.Reflection.Assembly executingAssembly = System.Reflection.Assembly.GetExecutingAssembly();
        logger.LogInformationMessage( $"Starting {executingAssembly.FullName}." );
        logger.LogInformationMessage( $"  running under {framework?.FrameworkName}." );


        // entry to run the application
        serviceProvider.GetService<App>().Run();
    }

    #endregion
}

#region " logger-injected application classes "

/// <summary>
/// To maintain a separation of concern between our business based logic and the logic we use to
/// configure and run the actual console application, let's create a new class called App.cs. The
/// goal being that we will use Program.cs to bootstrap everything need to support our
/// application and then fire off all the logic that is needed to support out "business" needs by
/// way of executing the Run() method in App.cs.
/// </summary>
/// <remarks>   David, 2021-02-04. </remarks>
public class App( ITestService testService, ILogger<App> logger )
{
    private readonly ITestService _testService = testService;
    private readonly ILogger<App> _logger = logger;

    public void Run()
    {
        this._logger.LogInformationMessage( "Running application." );

        TimeSpan timeout = TimeSpan.FromMilliseconds( 100 );
        bool fileOpened = HeaderWriter.AwaitFullFileName( timeout, TimeSpan.FromMilliseconds( 20 ), TimeSpan.FromMilliseconds( 20 ) );
        string message = fileOpened
            ? $"Logging into {HeaderWriter.FullFileName}."
            : $"File not open after {timeout.TotalMilliseconds}ms.";
        this._logger.LogInformationMessage( $"{message}" );
        try
        {
            this._testService.Run();
        }
        catch ( Exception ex )
        {
            InvalidOperationException reportEx = new( $"Caught in {nameof( App )}", ex );
            this._logger.LogExceptionMessage( "reporting exception", reportEx );
        }
        Log.CloseAndFlush();
    }
}
/// <summary>   Interface for test service. </summary>
/// <remarks>   David, 2021-02-04. </remarks>
public interface ITestService
{
    void Run();
}
/// <summary>   A service for accessing tests information. </summary>
/// <remarks>   David, 2021-02-04. </remarks>
internal sealed class TestService( ILogger<TestService> logger ) : ITestService
{
    private readonly ILogger<TestService> _logger = logger;

    public void Run()
    {
        this._logger.LogDebugMessage( "Running logging service." );
        this._logger.LogDebugMessage( "Throwing divide by zero exception." );
        throw new DivideByZeroException();
    }
}

#endregion
