using System.Diagnostics;
using cc.isr.Logging.Extensions;

namespace cc.isr.Logging.TraceLog.Tests;

/// <summary>   (Unit Test Class) an observable logger base tests. </summary>
/// <remarks>   David, 2020-09-22. </remarks>
[TestClass]
public class ObservableLoggerBaseTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.Name}";
        try
        {
            SiteSettings = TestSiteSettings.Instance;
            TraceListener = new Tracing.TestWriterQueueTraceListener( $"{testContext.FullyQualifiedTestClassName}.TraceListener",
                SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch ( Exception ex )
        {
            Trace.WriteLine( $"Failed initializing the test class: {ex}", methodFullName );

            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    private IDisposable? _loggerScope;

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {TimeZoneInfo.Local}" );
        Console.WriteLine( $"Testing {typeof( LogLevelSelector ).Assembly.FullName}" );

        this._loggerScope = Logger?.BeginScope( this.TestContext?.TestName ?? string.Empty );
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        this._loggerScope?.Dispose();
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    /// <summary>   Gets a logger instance for this category. </summary>
    /// <value> The logger. </value>
    public static ILogger<ObservableLoggerBaseTests>? Logger { get; } = LoggerProvider.CreateLogger<ObservableLoggerBaseTests>();

    /// <summary>   Gets the site settings. </summary>
    /// <value> The site settings. </value>
    private static TestSiteSettings? SiteSettings { get; set; }

    /// <summary>   Gets the assembly file info for getting the test settings
    /// files for this test project</summary>
    /// <value> The AssemblyFileInfo. </value>
    public static AssemblyFileInfo? AssemblyFileInfo { get; set; }

    /// <summary>   Gets or sets the scribe. </summary>
    /// <value> The scribe. </value>
    public static AppSettingsScribe? Scribe { get; set; }

    #endregion

    #region " initialization tests "

    /// <summary>   (Unit Test Method) tests 00 class initialize. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod]
    public void ClassInitializeTest()
    {
        Assert.IsNotNull( SiteSettings, $"{nameof( SiteSettings )} should initialize" );

        Assert.IsTrue( SiteSettings.HostInfoIndex() >= 0,
            $"{nameof( TestSiteSettings.HostAddress )} should be identified" );

        Assert.IsNotNull( Logger, $"{nameof( Logger )} should initialize" );

        Assert.IsTrue( Logger.IsEnabled( LogLevel.Information ),
            $"{nameof( Logger )} should be enabled for the {LogLevel.Information} {nameof( LogLevel )}" );
    }

    #endregion

    #region " observable logger change event handling tests "

    /// <summary>   (Unit Test Class) an observable logger. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    private sealed class ObservableLoggerBaseInstance : ObservableLoggerBase
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   2024-07-29. </remarks>
        public ObservableLoggerBaseInstance() : base( new TraceLogger<ObservableLoggerBase>() ) => this.TraceLogger.CreateSerilogLogger( typeof( ObservableLoggerBaseInstance ) );

        /// <summary> The expected value. </summary>
        private string? _expectedValue;

        /// <summary> Gets or sets the expected value. </summary>
        /// <value> The expected value. </value>
        public string? ExpectedValue
        {
            get => this._expectedValue;
            set => this.SetProperty( ref this._expectedValue, value );
        }

        /// <summary> Gets or sets the actual value. </summary>
        /// <value> The actual value. </value>
        public string? ActualValue { get; set; }

        protected override bool AddExceptionData( Exception ex )
        {
            return true;
        }

        /// <summary>
        /// Executes the exception and publish on a different thread, and waits for the result.
        /// </summary>
        /// <remarks>   David, 2021-06-30. </remarks>
        /// <returns>   A Tuple. </returns>
        public (bool Success, string Details) InvokeExceptionAndPublish()
        {
            try
            {
                this.TraceLogger.Logger?.LogVerboseMessage( "Before getting log file name" );
                Console.WriteLine( $"Log file name: {Orlog.TraceLogBrowser.FullLogFileName}" );
                throw new DivideByZeroException();
            }
            catch ( DivideByZeroException ex )
            {
                try
                {
                    _ = this.LogError( ex, string.Empty );
                    return (true, string.Empty);
                }
                catch ( Exception ex2 )
                {
                    return (false, $"Exception occurred publishing exception {ex}; details: {ex2}");
                }
            }
        }

    }

    /// <summary> Handles the property changed. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Property changed event information. </param>
    private void HandlePropertyChanged( object? sender, System.ComponentModel.PropertyChangedEventArgs e )
    {
        if ( sender is not ObservableLoggerBaseInstance propertyChangeSender )
            return;

        switch ( e?.PropertyName ?? "" )
        {
            case nameof( ObservableLoggerBaseInstance.ExpectedValue ):
                {
                    propertyChangeSender.ActualValue = propertyChangeSender.ExpectedValue;
                    break;
                }

            default:
                break;
        }
    }

    /// <summary>   (Unit Test Method) property change should be handled. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    [TestMethod()]
    public void PropertyChangeShouldBeHandled()
    {
        ObservableLoggerBaseInstance sender = new();
        sender.PropertyChanged += this.HandlePropertyChanged;
        sender.ExpectedValue = nameof( ObservableLoggerBaseInstance.ExpectedValue );
        Assert.AreEqual( sender.ExpectedValue, sender.ActualValue, "should equal after handling the synchronized event" );
    }

    /// <summary>
    /// (Unit Test Method) observable logger base instance should be constructed.
    /// </summary>
    /// <remarks>   David, 2020-10-10. <para>
    /// The following output requires using the Serilog logging settings file.
    /// </para> <code>
    /// Standard Output: 
    /// cc.isr.Logging.TraceLog.MSTest.ObservableLoggerBaseTests
    /// c::12:18:20.528 -07:00, [INF], (cc.isr.Logging.TraceLog.Tracer.Instance), Before getting log file name
    /// Log file name:
    /// c::12:18:20.540 -07:00, [ERR], (cc.isr.Logging.TraceLog.Tracer.Instance), [C:\my\lib\vs\core\logging\src\TraceLog\TraceLog.MSTest\ObservableLoggerBaseTests.cs].InvokeExceptionAndPublish.Line#176,
    /// System.DivideByZeroException: Attempted to divide by zero.
    ///       at cc.isr.Logging.TraceLog.MSTest.ObservableLoggerBaseTests.ObservableLoggerBaseInstance.InvokeExceptionAndPublish() in C:\my\lib\vs\core\logging\src\TraceLog\TraceLog.MSTest\ObservableLoggerBaseTests.cs:line 170
    /// 1->    Type: System.DivideByZeroException
    /// 1-> Message: Attempted to divide by zero.
    /// 1->  Source: cc.isr.Logging.TraceLog.MSTest
    /// 1->  Method: System.ValueTuple`2[System.Boolean, System.String] InvokeExceptionAndPublish()
    /// 1-> HResult: -2147352558 (80020012)
    ///
    /// Debug Trace:
    /// testHost Information: 0 : t::12:18:20.528 -07:00, [INF], (cc.isr.Logging.TraceLog.Tracer.Instance), Before getting log file name
    /// d::12:18:20.528 -07:00, [INF], (cc.isr.Logging.TraceLog.Tracer.Instance), Before getting log file name
    /// testHost Error: 0 : t::12:18:20.540 -07:00, [ERR], (cc.isr.Logging.TraceLog.Tracer.Instance), [C:\my\lib\vs\core\logging\src\TraceLog\TraceLog.MSTest\ObservableLoggerBaseTests.cs].InvokeExceptionAndPublish.Line#176,
    /// System.DivideByZeroException: Attempted to divide by zero.
    ///   at cc.isr.Logging.TraceLog.MSTest.ObservableLoggerBaseTests.ObservableLoggerBaseInstance.InvokeExceptionAndPublish() in C:\my\lib\vs\core\logging\src\TraceLog\TraceLog.MSTest\ObservableLoggerBaseTests.cs:line 170
    /// 1->    Type: System.DivideByZeroException
    /// 1-> Message: Attempted to divide by zero.
    /// 1->  Source: cc.isr.Logging.TraceLog.MSTest
    /// 1->  Method: System.ValueTuple`2[System.Boolean, System.String] InvokeExceptionAndPublish()
    /// 1-> HResult: -2147352558 (80020012)
    /// d::12:18:20.540 -07:00, [ERR], (cc.isr.Logging.TraceLog.Tracer.Instance), [C:\my\lib\vs\core\logging\src\TraceLog\TraceLog.MSTest\ObservableLoggerBaseTests.cs].InvokeExceptionAndPublish.Line#176,
    /// System.DivideByZeroException: Attempted to divide by zero.
    ///   at cc.isr.Logging.TraceLog.MSTest.ObservableLoggerBaseTests.ObservableLoggerBaseInstance.InvokeExceptionAndPublish() in C:\my\lib\vs\core\logging\src\TraceLog\TraceLog.MSTest\ObservableLoggerBaseTests.cs:line 170
    /// 1->    Type: System.DivideByZeroException
    /// 1-> Message: Attempted to divide by zero.
    /// 1->  Source: cc.isr.Logging.TraceLog.MSTest
    /// 1->  Method: System.ValueTuple`2[System.Boolean, System.String] InvokeExceptionAndPublish()
    /// 1-> HResult: -2147352558 (80020012)
    ///
    ///
    /// </code></remarks>
    [TestMethod()]
    public void ObservableLoggerBaseInstanceShouldBeConstructed()
    {
        ObservableLoggerBaseInstance observableLoggerBase = new();
        (bool success, string details) = observableLoggerBase.InvokeExceptionAndPublish();
        Assert.IsTrue( success, details );
        if ( TraceListener is not null && TraceListener.Queue is not null && !TraceListener.Queue.IsEmpty )
            TraceListener.ClearQueue();
    }

    #endregion
}
