using System.ComponentModel;
using System.Text.Json.Serialization;

namespace cc.isr.Logging.TraceLog;

/// <summary>   A settings. </summary>
/// <remarks>   2024-10-16. </remarks>
public class TraceLogSettings : INotifyPropertyChanged
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2023-05-09. </remarks>
    public TraceLogSettings()
    { }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<T>( ref T backingField, T value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<T>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<T>( ref T prop, T value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
        if ( EqualityComparer<T>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<T>( T oldValue, T newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<T>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " settings properties "

    private bool _exists;

    /// <summary>
    /// Gets or sets a value indicating whether this settings section exists and the values were thus
    /// fetched from the settings file.
    /// </summary>
    /// <value> True if this settings section exists in the settings file, false if not. </value>
	[System.ComponentModel.Description( "True if this settings were found and read from the settings file." )]
    public bool Exists
    {
        get => this._exists;
        set => _ = this.SetProperty( ref this._exists, value );
    }

    #endregion

    #region " log levels "

    private System.Diagnostics.TraceLevel _messageLevel = System.Diagnostics.TraceLevel.Off;

    /// <summary>   Gets or sets the message trace level. </summary>
    /// <remarks>
    /// This property name is different from the <see cref="System.Text.Json"/> property name in
    /// order to ensure that the class is correctly serialized. It's value is initialized as <see cref="System.Diagnostics.TraceLevel.Off"/>
    /// so as to be able to ensure that the reading came from the settings file.
    /// </remarks>
    /// <value> The message <see cref="System.Diagnostics.TraceLevel"/>. </value>
    [System.ComponentModel.Description( "Sets the message trace level" )]
    [JsonPropertyName( "TraceLevel" )]
    public System.Diagnostics.TraceLevel MessageLevel
    {
        get => this._messageLevel;
        set => _ = this.SetProperty( ref this._messageLevel, value );
    }

    private Microsoft.Extensions.Logging.LogLevel _applicationLogLevel = Microsoft.Extensions.Logging.LogLevel.Trace;

    /// <summary>   Gets or sets the application log level. </summary>
    /// <remarks>
    /// The minimal level for logging events at the application level. The logger logs the message if
    /// the message level is the same or higher than this level.
    /// </remarks>
    /// <value> The application log level. </value>
	[Description( "The minimal level for logging events at the application level. The logger logs the message if the message level is the same or higher than this level" )]
    public Microsoft.Extensions.Logging.LogLevel ApplicationLogLevel
    {
        get => this._applicationLogLevel;
        set => this.SetProperty( ref this._applicationLogLevel, value );
    }

    private Microsoft.Extensions.Logging.LogLevel _assemblyLogLevel = Microsoft.Extensions.Logging.LogLevel.Trace;

    /// <summary>   Gets or sets the assembly log level. </summary>
    /// <remarks>
    /// The minimum log level for sending the message to the logger by this assembly. This
    /// filters the message before it is sent to the Logging program.
    /// </remarks>
    /// <value> The assembly log level. </value>
	[Description( "The minimum log level for sending the message to the logger by this assembly. This filters the message before it is sent to the Logging program." )]
    public Microsoft.Extensions.Logging.LogLevel AssemblyLogLevel
    {
        get => this._assemblyLogLevel;
        set => this.SetProperty( ref this._assemblyLogLevel, value );
    }

    #endregion

    #region " trace Levels "

    private System.Diagnostics.TraceEventType _traceLogLevel = System.Diagnostics.TraceEventType.Information;

    /// <summary>   Gets or sets the trace log level. </summary>
    /// <value> The trace log level. </value>
	[Description( "The maximum trace event type for logging events. Only messages with a message level that is same or higher than this level are logged [Information; 8]." )]
    public System.Diagnostics.TraceEventType TraceLogLevel
    {
        get => this._traceLogLevel;
        set => this.SetProperty( ref this._traceLogLevel, value );
    }

    private System.Diagnostics.TraceEventType _traceShowLevel = System.Diagnostics.TraceEventType.Information;

    /// <summary>   Gets or sets the display level for log and trace messages. </summary>
    /// <remarks>
    /// The maximum trace event type for displaying logged and trace events. Only messages with a
    /// message <see cref="System.Diagnostics.TraceEventType"/> level that is same or higher than
    /// this level are displayed.
    /// </remarks>
    /// <value> The message display level. </value>
	[Description( "The maximum trace event type for displaying logged and trace events. Only messages with a message level that is same or higher than this level are displayed [Information; 8]." )]
    public System.Diagnostics.TraceEventType TraceShowLevel
    {
        get => this._traceShowLevel;
        set => this.SetProperty( ref this._traceShowLevel, value );
    }

    #endregion
}
