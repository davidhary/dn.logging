using System.ComponentModel;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
using cc.isr.Logging.Extensions;

namespace cc.isr.Logging.TraceLog;

/// <summary>   A trace logger for emitting messages to file, trace and debug listeners. </summary>
/// <remarks>
/// David, 2021-02-23. <para>
/// Writing from the Microsoft ILogger works; writing from the Serilog Log Logger does not work.
/// See Platform/TraceLogger.cs.
/// </para>
/// </remarks>
public partial class TraceLogger<TCategory> : INotifyPropertyChanged
{
    #region " construction and cleanup "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2024-07-30. </remarks>
    public TraceLogger() { }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<T>( ref T backingField, T value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<T>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<T>( ref T prop, T value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
        if ( EqualityComparer<T>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<T>( T oldValue, T newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<T>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " logger "

    /// <summary>   Gets the logger. which emits messages to file, trace (warnings and errors), and debug (fatal messages only). </summary>
    /// <value> The logger. </value>
    public ILogger<TCategory>? Logger { get; private set; }

    /// <summary>   Creates a Serilog logger. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <param name="settingsFileName"> Filename of the settings file. </param>
    public void CreateSerilogLogger( string settingsFileName )
    {
        // the logger logs the messages to a file, trace and debug
        this.Logger = cc.isr.Logging.Orlog.Orlogger.Instance.CreateSerilogLogger<TCategory>( settingsFileName )!;
    }

    /// <summary>   Creates a Serilog logger. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <param name="assembly"> The assembly. </param>
    public void CreateSerilogLogger( System.Reflection.Assembly assembly )
    {
        // the logger logs the messages to a file, trace and debug
        this.Logger = cc.isr.Logging.Orlog.Orlogger.Instance.CreateSerilogLogger<TCategory>( assembly )!;
    }

    /// <summary>   Creates a Serilog logger. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <param name="callingAssemblyElementType">   Type of the calling assembly element. </param>
    public void CreateSerilogLogger( Type callingAssemblyElementType )
    {
        // the logger logs the messages to a file, trace and debug
        this.Logger = cc.isr.Logging.Orlog.Orlogger.Instance.CreateSerilogLogger<TCategory>( callingAssemblyElementType )!;
    }

    /// <summary>   Reset the Serilog Log Logger to default and dispose the original if possible.. </summary>
    /// <remarks>   David, 2021-03-13. </remarks>
    public static void CloseAndFlush()
    {
        cc.isr.Logging.Orlog.Orlogger.CloseAndFlush();
    }

    /// <summary>   Return a a Serilog.ILogger that adds logging context. </summary>
    /// <remarks>   David, 2021-07-02. 
    /// <see href="https://StackOverflow.com/questions/29470863/SERILOG-output-enrich-all-messages-with-MethodName-from-which-log-entry-was-ca"/>.
    /// </remarks>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A Serilog.ILogger. </returns>
    public static Serilog.ILogger GetContextLogger( [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                                    [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
                                                    [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        return Serilog.Log.Logger
            .ForContext( "MemberName", memberName )
            .ForContext( "FilePath", sourceFilePath )
            .ForContext( "LineNumber", sourceLineNumber );
    }

    #endregion

    #region " logging "

    /// <summary>
    /// Writes a trace message (same as verbose) with caller information. Contains the most detailed
    /// messages. These messages may contain sensitive application data. These messages are disabled
    /// by default and should never be enabled in a production environment.
    /// </summary>
    /// <remarks>   This is a left over from using trace event types to manage logging. </remarks>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public string LogTrace( string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        this.Logger?.LogTraceMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
    }

    /// <summary>
    /// Writes a verbose (trace for the Microsoft ILogger) message with caller information. Contains
    /// the most detailed messages. These messages may contain sensitive application data. These
    /// messages are disabled by default and should never be enabled in a production environment.
    /// </summary>
    /// <remarks>   This is a left over from using trace event types to manage logging. </remarks>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public string LogVerbose( string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        this.Logger?.LogVerboseMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Debug message with caller information. Used for interactive investigation during
    /// development. These logs should primarily contain information useful for debugging and have no
    /// long-term value.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public string LogDebug( string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        this.Logger?.LogDebugMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Information message with caller information. Logs that track the general flow of the
    /// application. These logs should have long-term value.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public string LogInformation( string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        this.Logger?.LogInformationMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Warning message with caller information. Logs that highlight an abnormal or
    /// unexpected event in the application flow, but do not otherwise cause the application
    /// execution to stop.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public string LogWarning( string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        this.Logger?.LogWarningMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Error message with caller information. Logs that highlight when the current flow of
    /// execution is stopped due to a failure. These should indicate a failure in the current
    /// activity, not an application-wide failure.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public string LogError( string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        this.Logger?.LogErrorMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Error message with caller information. Logs that highlight when the current flow of
    /// execution is stopped due to a failure. These should indicate a failure in the current
    /// activity, not an application-wide failure.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="ex">               The exception. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public string LogException( Exception ex, string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        this.Logger?.LogExceptionMultiLineMessage( message, ex, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Critical (Fatal) Serilog message with caller information. Logs that describe an
    /// unrecoverable application or system crash, or a catastrophic failure that requires immediate
    /// attention.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public string LogCritical( string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        this.Logger?.LogCriticalMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Fatal (Critical for the Microsoft ILogger) message  with caller information.
    /// Logs that describe an unrecoverable application or system crash, or a catastrophic
    /// failure that requires immediate attention.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public string LogFatal( string message,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        this.Logger?.LogFatalMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Write a log message with caller information using the <see cref="Logger"/>
    /// <see cref="ILogger{TraceLogger}"/>.
    /// </summary>
    /// <remarks>   David, 2021-03-13. </remarks>
    /// <param name="messageLevel">     The logged message level. </param>
    /// <param name="message">          The message. </param>
    /// <param name="minimumLevel">     (Optional) The minimum level. The message is passed to the
    ///                                 logger if the <paramref name="messageLevel"/> is the same or
    ///                                 higher than this level. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public string LogCallerMessage( LogLevel messageLevel, string message, LogLevel minimumLevel = LogLevel.Trace,
                                    [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                    [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
                                    [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        return messageLevel < minimumLevel
            ? message
            : messageLevel switch
            {
                LogLevel.Trace => this.LogTrace( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Debug => this.LogDebug( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Information => this.LogInformation( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Warning => this.LogWarning( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Error => this.LogError( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Critical => this.LogCritical( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.None => string.Empty,
                _ => this.LogInformation( message, memberName, sourcePath, sourceLineNumber ),
            };
    }

    /// <summary>
    /// Write a log message using the <see cref="Logger"/>
    /// <see cref="ILogger{TraceLogger}"/>.
    /// </summary>
    /// <remarks>   David, 2022-02-09. </remarks>
    /// <param name="level">    The level. </param>
    /// <param name="message">  The message. </param>
    /// <returns>   A <see cref="string" />. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0010:Add missing cases", Justification = "<Pending>" )]
    public string LogEventType( TraceEventType level, string message )
    {
        switch ( level )
        {
            case TraceEventType.Information:
                this.Logger?.LogInformationMessage( message );
                break;
            case TraceEventType.Warning:
                this.Logger?.LogWarningMessage( message );
                break;
            case TraceEventType.Error:
                this.Logger?.LogErrorMessage( message );
                break;
            case TraceEventType.Critical:
                this.Logger?.LogCriticalMessage( message );
                break;
            case TraceEventType.Verbose:
                this.Logger?.LogVerboseMessage( message );
                break;
            default:
                this.Logger?.LogInformationMessage( message );
                break;
        }
        return message;
    }

    /// <summary>   Write a log message using the <see cref="Logger"/> <see cref="ILogger{TraceLogger}"/>. </summary>
    /// <remarks>   David, 2021-03-13. </remarks>
    /// <param name="level">    The level. </param>
    /// <param name="message">  The message. </param>
    public string LogMessage( LogLevel level, string message )
    {
        switch ( level )
        {
            case LogLevel.Trace:
                this.Logger?.LogTraceMessage( message );
                break;
            case LogLevel.Debug:
                this.Logger?.LogDebugMessage( message );
                break;
            case LogLevel.Information:
                this.Logger?.LogInformationMessage( message );
                break;
            case LogLevel.Warning:
                this.Logger?.LogWarningMessage( message );
                break;
            case LogLevel.Error:
                this.Logger?.LogErrorMessage( message );
                break;
            case LogLevel.Critical:
                this.Logger?.LogCriticalMessage( message );
                break;
            case LogLevel.None:
                break;
            default:
                this.Logger?.LogInformationMessage( message );
                break;
        }
        return message;
    }

    #endregion

    #region " logging and trace event writer log levels "

    private KeyValuePair<LogLevel, string> _loggingLevelValueNamePair;

    /// <summary>
    /// Gets or sets the <see cref="LogLevel"/> value name pair for logging. This level determines
    /// the level of the <see cref="ILogger"/>.
    /// </summary>
    /// <value> The log level value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<LogLevel, string> LoggingLevelValueNamePair
    {
        get => this._loggingLevelValueNamePair;
        set
        {
            if ( this.SetProperty( ref this._loggingLevelValueNamePair, value ) )
            {
                this.MinimumLogLevel = value.Key;
            }
        }
    }

    /// <summary>
    /// Gets or sets the <see cref="ILogger"/> minimum runtime <see cref="LogLevel"/>.
    /// </summary>
    /// <remarks>
    /// Events with a log level lower than both the <see cref="MinimumLogLevel"/> and the initial
    /// Serilog Logger log level, as set when initializing the Serilog Logger, will be logged.
    /// </remarks>
    /// <value> The log level. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public LogLevel MinimumLogLevel
    {
        get => cc.isr.Logging.Orlog.Orlogger.Instance.RuntimeSerilogLevel;
        set
        {
            if ( this.SetProperty( cc.isr.Logging.Orlog.Orlogger.Instance.RuntimeSerilogLevel, value,
                () => cc.isr.Logging.Orlog.Orlogger.Instance.RuntimeSerilogLevel = value ) )
            {
                this.LoggingLevelValueNamePair = LogLevelSelector.ToLogLevelValueNamePair( value );
            }
        }
    }

    #endregion

    #region " trace event writer trace event "

    private KeyValuePair<TraceEventType, string> _traceEventWriterTraceEventValueNamePair;

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> value name pair for the global trace event
    /// writer. This level determines the level of all the
    /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
    /// lower level.
    /// </summary>
    /// <value> The trace event writer trace event value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<TraceEventType, string> TraceEventWriterTraceEventValueNamePair
    {
        get => this._traceEventWriterTraceEventValueNamePair;
        set
        {
            if ( this.SetProperty( ref this._traceEventWriterTraceEventValueNamePair, value ) )
            {
                this.TraceEventWriterTraceEventType = value.Key;
            }
        }
    }

    private TraceEventType _traceEventWriterTraceEventType;

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> for the global trace event writer. This level
    /// determines the level of all the
    /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
    /// lower level.
    /// </summary>
    /// <value> The type of the trace event writer trace event. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType TraceEventWriterTraceEventType
    {
        get => this._traceEventWriterTraceEventType;
        // deprecated: Trace event types do not map to log levels.
        // TraceLogger<TCategory>.ToTraceEventType( this.TraceEventWriterLogLevel );
        set
        {
            if ( this.SetProperty( ref this._traceEventWriterTraceEventType, value ) )
            {
                // deprecated; Log levels do not map to trace
                // event types. this.TraceEventWriterLogLevel = TraceLogger<TCategory>.ToLogLevel( value );
                this.TraceEventWriterTraceEventValueNamePair = TraceEventSelector.ToTraceEventValueNamePair( value );
            }
        }
    }

    #endregion
}
