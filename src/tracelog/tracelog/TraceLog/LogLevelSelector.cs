using Microsoft.Extensions.Logging;

namespace cc.isr.Logging.TraceLog;

/// <summary>   A log level selector. </summary>
/// <remarks>   2024-07-29. </remarks>
public static class LogLevelSelector
{
    #region " log levels: "

    private static List<LogLevel>? _logLevels;

    /// <summary>   Gets the log levels. </summary>
    /// <value> The log levels. </value>
    public static IEnumerable<LogLevel> LogLevels
    {
        get
        {
            if ( LogLevelSelector._logLevels is null || !LogLevelSelector._logLevels.Any() )
            {
                LogLevelSelector._logLevels = [LogLevel.Critical, LogLevel.Error, LogLevel.Warning, LogLevel.Information, LogLevel.Debug, LogLevel.Trace];
            }
            return LogLevelSelector._logLevels;
        }
    }

    private static InvokingBindingList<LogLevel>? _logLevelBindingList;

    /// <summary>   Gets a list of log level bindings. </summary>
    /// <value> A list of log level bindings. </value>
    public static InvokingBindingList<LogLevel> LogLevelBindingList
    {
        get
        {
            if ( LogLevelSelector._logLevelBindingList is null || !LogLevelSelector._logLevelBindingList.Any() )
            {
                LogLevelSelector._logLevelBindingList = new InvokingBindingList<LogLevel>( LogLevelSelector.LogLevels.ToArray() );
            }

            return LogLevelSelector._logLevelBindingList;
        }
    }

    private static InvokingBindingList<LogLevel>? _showLevelBindingList;

    /// <summary>   Gets a list of Show level bindings. </summary>
    /// <value> A list of Show level bindings. </value>
    public static InvokingBindingList<LogLevel> ShowLevelBindingList
    {
        get
        {
            if ( LogLevelSelector._showLevelBindingList is null || !LogLevelSelector._showLevelBindingList.Any() )
            {
                LogLevelSelector._showLevelBindingList = new InvokingBindingList<LogLevel>( LogLevelSelector.LogLevels.ToArray() );
            }
            return LogLevelSelector._showLevelBindingList;
        }
    }

    /// <summary>   The log level value name pairs. </summary>
    private static List<KeyValuePair<LogLevel, string>>? _logLevelValueNamePairs;

    /// <summary>   Gets the log level value name pairs. </summary>
    /// <value> The log level value name pairs. </value>
    public static IEnumerable<KeyValuePair<LogLevel, string>> LogLevelValueNamePairs
    {
        get
        {
            if ( LogLevelSelector._logLevelValueNamePairs is null || !LogLevelSelector._logLevelValueNamePairs.Any() )
            {
                LogLevelSelector._logLevelValueNamePairs = [ ToLogLevelValueNamePair( LogLevel.Critical ),
                                                             ToLogLevelValueNamePair( LogLevel.Error ),
                                                             ToLogLevelValueNamePair( LogLevel.Warning ),
                                                             ToLogLevelValueNamePair( LogLevel.Information ),
                                                             ToLogLevelValueNamePair( LogLevel.Debug ),
                                                             ToLogLevelValueNamePair( LogLevel.Trace )  ];
            }

            return LogLevelSelector._logLevelValueNamePairs;
        }
    }

    /// <summary>   Converts a value to a log level value name pair. </summary>
    /// <remarks>   David, 2020-09-21. </remarks>
    /// <param name="value">    The value. </param>
    /// <returns>   Value as a KeyValuePair(Of LogLevel, String) </returns>
    public static KeyValuePair<LogLevel, string> ToLogLevelValueNamePair( LogLevel value )
    {
        return new KeyValuePair<LogLevel, string>( value, value.ToString() );
    }

    private static InvokingBindingList<KeyValuePair<LogLevel, string>>? _logLevelValueNamePairBindingList;

    /// <summary>   Gets a list of log level value name pair for bindings for selecting the log level. </summary>
    /// <value> A list of log level value name pair for bindings. </value>
    public static InvokingBindingList<KeyValuePair<LogLevel, string>> LogLevelValueNamePairBindingList
    {
        get
        {
            if ( LogLevelSelector._logLevelValueNamePairBindingList is null || !LogLevelSelector._logLevelValueNamePairBindingList.Any() )
            {
                LogLevelSelector._logLevelValueNamePairBindingList = new InvokingBindingList<KeyValuePair<LogLevel, string>>( LogLevelValueNamePairs.ToArray() );
            }
            return LogLevelSelector._logLevelValueNamePairBindingList;
        }
    }

    private static InvokingBindingList<KeyValuePair<LogLevel, string>>? _showLevelValueNamePairBindingList;

    /// <summary>   Gets a list of log level value name pair for bindings for selecting the show level. </summary>
    /// <value> A list of log level value name pair for bindings. </value>
    public static InvokingBindingList<KeyValuePair<LogLevel, string>> ShowLevelValueNamePairBindingList
    {
        get
        {
            if ( LogLevelSelector._showLevelValueNamePairBindingList is null || !LogLevelSelector._showLevelValueNamePairBindingList.Any() )
            {
                LogLevelSelector._showLevelValueNamePairBindingList = new InvokingBindingList<KeyValuePair<LogLevel, string>>( LogLevelSelector.LogLevelValueNamePairs.ToArray() );
            }
            return LogLevelSelector._showLevelValueNamePairBindingList;
        }
    }

    #endregion
}
