using System.ComponentModel;
using System.Diagnostics;

namespace cc.isr.Logging.TraceLog;

/// <summary>   A trace event selector. </summary>
/// <remarks>   2024-07-29. </remarks>
public static class TraceEventSelector
{
    #region " trace events: "

    private static List<TraceEventType>? _traceEventTypes;

    /// <summary>   Gets a list of types of the trace events. </summary>
    /// <value> A list of types of the trace events. </value>
    public static IEnumerable<TraceEventType> TraceEventTypes
    {
        get
        {
            if ( TraceEventSelector._traceEventTypes is null || !TraceEventSelector._traceEventTypes.Any() )
            {
                TraceEventSelector._traceEventTypes = [TraceEventType.Critical, TraceEventType.Error, TraceEventType.Warning, TraceEventType.Information, TraceEventType.Verbose];
            }
            return TraceEventSelector._traceEventTypes;
        }
    }

    private static InvokingBindingList<TraceEventType>? _logTraceEventTypeBindingList;

    /// <summary>   Gets a list of log trace event type bindings. </summary>
    /// <value> A list of log trace event type bindings. </value>
    public static InvokingBindingList<TraceEventType> LogTraceEventTypeBindingList
    {
        get
        {
            if ( TraceEventSelector._logTraceEventTypeBindingList is null || !TraceEventSelector._logTraceEventTypeBindingList.Any() )
            {
                TraceEventSelector._logTraceEventTypeBindingList = new InvokingBindingList<TraceEventType>( TraceEventSelector.TraceEventTypes.ToArray() );
            }

            return TraceEventSelector._logTraceEventTypeBindingList;
        }
    }

    private static InvokingBindingList<TraceEventType>? _showTraceEventTypeBindingList;

    /// <summary>   Gets a list of show trace event type bindings. </summary>
    /// <value> A list of show trace event type bindings. </value>
    public static InvokingBindingList<TraceEventType> ShowTraceEventTypeBindingList
    {
        get
        {
            if ( TraceEventSelector._showTraceEventTypeBindingList is null || !TraceEventSelector._showTraceEventTypeBindingList.Any() )
            {
                TraceEventSelector._showTraceEventTypeBindingList = new InvokingBindingList<TraceEventType>( TraceEventSelector.TraceEventTypes.ToArray() );
            }

            return TraceEventSelector._showTraceEventTypeBindingList;
        }
    }

    private static List<KeyValuePair<TraceEventType, string>>? _traceEventValueNamePairs;

    /// <summary>   Gets the trace event value name pairs. </summary>
    /// <value> The trace event value name pairs. </value>
    public static IEnumerable<KeyValuePair<TraceEventType, string>> TraceEventValueNamePairs
    {
        get
        {
            if ( TraceEventSelector._traceEventValueNamePairs is null || !TraceEventSelector._traceEventValueNamePairs.Any() )
            {
                TraceEventSelector._traceEventValueNamePairs = [ ToTraceEventValueNamePair( TraceEventType.Critical ),
                                                                 ToTraceEventValueNamePair( TraceEventType.Error ),
                                                                 ToTraceEventValueNamePair( TraceEventType.Warning ),
                                                                 ToTraceEventValueNamePair( TraceEventType.Information ),
                                                                 ToTraceEventValueNamePair( TraceEventType.Verbose ) ];
            }

            return TraceEventSelector._traceEventValueNamePairs;
        }
    }

    /// <summary> Converts a value to a trace event value name pair. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="value"> The value. </param>
    /// <returns> Value as a KeyValuePair(Of TraceEventType, String) </returns>
    public static KeyValuePair<TraceEventType, string> ToTraceEventValueNamePair( TraceEventType value )
    {
        return new KeyValuePair<TraceEventType, string>( value, value.ToString() );
    }

    private static InvokingBindingList<KeyValuePair<TraceEventType, string>>? _logTraceEventValueNamePairBindingList;

    /// <summary>   Gets a list of trace event value name pair bindings for selecting the log trace event level. </summary>
    /// <value> A list of trace event value name pair bindings. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public static InvokingBindingList<KeyValuePair<TraceEventType, string>> LogTraceEventValueNamePairBindingList
    {
        get
        {
            if ( TraceEventSelector._logTraceEventValueNamePairBindingList is null || !TraceEventSelector._logTraceEventValueNamePairBindingList.Any() )
            {
                TraceEventSelector._logTraceEventValueNamePairBindingList = new InvokingBindingList<KeyValuePair<TraceEventType, string>>( TraceEventSelector.TraceEventValueNamePairs.ToArray() );
            }
            return TraceEventSelector._logTraceEventValueNamePairBindingList;
        }
    }

    private static InvokingBindingList<KeyValuePair<TraceEventType, string>>? _showTraceEventValueNamePairBindingList;

    /// <summary>   Gets a list of trace event value name pair bindings for selecting the Show trace event level. </summary>
    /// <value> A list of trace event value name pair bindings. </value>
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    [Browsable( false )]
    public static InvokingBindingList<KeyValuePair<TraceEventType, string>> ShowTraceEventValueNamePairBindingList
    {
        get
        {
            if ( TraceEventSelector._showTraceEventValueNamePairBindingList is null || !TraceEventSelector._showTraceEventValueNamePairBindingList.Any() )
            {
                TraceEventSelector._showTraceEventValueNamePairBindingList = new InvokingBindingList<KeyValuePair<TraceEventType, string>>( TraceEventSelector.TraceEventValueNamePairs.ToArray() );
            }
            return TraceEventSelector._showTraceEventValueNamePairBindingList;
        }
    }

    #endregion
}
