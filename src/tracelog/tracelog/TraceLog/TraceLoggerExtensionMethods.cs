using System.Diagnostics;
using cc.isr.Logging.Extensions;
using Microsoft.Extensions.Logging;

namespace cc.isr.Logging.TraceLog;

/// <summary>   A trace log extension methods. </summary>
/// <remarks>   2024-07-29. </remarks>
public static class TraceLoggerExtensionMethods
{
    /// <summary>
    /// Writes a trace message (same as verbose) with caller information. Contains the most detailed
    /// messages. These messages may contain sensitive application data. These messages are disabled
    /// by default and should never be enabled in a production environment.
    /// </summary>
    /// <remarks>   This is a left over from using trace event types to manage logging. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string LogTrace<TCategory>( this TraceLogger<TCategory> traceLogger, string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                              [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
                                              [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        traceLogger.Logger?.LogTraceMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
    }

    /// <summary>
    /// Writes a verbose (trace for the Microsoft ILogger) message with caller information. Contains
    /// the most detailed messages. These messages may contain sensitive application data. These
    /// messages are disabled by default and should never be enabled in a production environment.
    /// </summary>
    /// <remarks>   This is a left over from using trace event types to manage logging. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string LogVerbose<TCategory>( this TraceLogger<TCategory> traceLogger, string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                                     [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
                                                     [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        traceLogger.Logger?.LogVerboseMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Debug message with caller information. Used for interactive investigation during
    /// development. These logs should primarily contain information useful for debugging and have no
    /// long-term value.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string LogDebug<TCategory>( this TraceLogger<TCategory> traceLogger, string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                                   [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
                                                   [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        traceLogger.Logger?.LogDebugMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Information message with caller information. Logs that track the general flow of the
    /// application. These logs should have long-term value.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string LogInformation<TCategory>( this TraceLogger<TCategory> traceLogger, string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                                         [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
                                                         [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        traceLogger.Logger?.LogInformationMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Warning message with caller information. Logs that highlight an abnormal or
    /// unexpected event in the application flow, but do not otherwise cause the application
    /// execution to stop.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string LogWarning<TCategory>( this TraceLogger<TCategory> traceLogger, string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                                     [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
                                                     [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        traceLogger.Logger?.LogWarningMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Error message with caller information. Logs that highlight when the current flow of
    /// execution is stopped due to a failure. These should indicate a failure in the current
    /// activity, not an application-wide failure.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string LogError<TCategory>( this TraceLogger<TCategory> traceLogger, string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                                   [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
                                                   [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        traceLogger.Logger?.LogErrorMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Error message with caller information. Logs that highlight when the current flow of
    /// execution is stopped due to a failure. These should indicate a failure in the current
    /// activity, not an application-wide failure.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="ex">               The exception. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string LogException<TCategory>( this TraceLogger<TCategory> traceLogger, string message, Exception ex,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        traceLogger.Logger?.LogExceptionMultiLineMessage( message, ex, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( ex, message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Critical (Fatal) Serilog message with caller information. Logs that describe an
    /// unrecoverable application or system crash, or a catastrophic failure that requires immediate
    /// attention.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string LogCritical<TCategory>( this TraceLogger<TCategory> traceLogger, string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                                      [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
                                                      [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        traceLogger.Logger?.LogCriticalMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Writes a Fatal (Critical for the Microsoft ILogger) message  with caller information.
    /// Logs that describe an unrecoverable application or system crash, or a catastrophic
    /// failure that requires immediate attention.
    /// </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string LogFatal<TCategory>( this TraceLogger<TCategory> traceLogger, string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                              [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
                                              [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        traceLogger.Logger?.LogFatalMultiLineMessage( message, memberName, sourcePath, sourceLineNumber );
        return cc.isr.Logging.Extensions.ILoggerExtensions.BuildMultiLineMessage( message, memberName, sourcePath, sourceLineNumber ); ;
    }

    /// <summary>
    /// Write a log message with caller information using the <see cref="Logger"/>
    /// <see cref="ILogger{TraceLogger}"/>.
    /// </summary>
    /// <remarks>   David, 2021-03-13. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="messageLevel">     The logged message level. </param>
    /// <param name="message">          The message. </param>
    /// <param name="minimumLevel">     (Optional) The minimum level. The message is passed to the
    ///                                 logger if the <paramref name="messageLevel"/> is the same or
    ///                                 higher than this level. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string LogCallerMessage<TCategory>( this TraceLogger<TCategory> traceLogger, LogLevel messageLevel, string message, LogLevel minimumLevel = LogLevel.Trace,
                                                      [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                                      [System.Runtime.CompilerServices.CallerFilePath] string sourcePath = "",
                                                      [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        return messageLevel < minimumLevel
            ? message
            : messageLevel switch
            {
                LogLevel.Trace => traceLogger.LogTrace( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Debug => traceLogger.LogDebug( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Information => traceLogger.LogInformation( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Warning => traceLogger.LogWarning( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Error => traceLogger.LogError( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Critical => traceLogger.LogCritical( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.None => string.Empty,
                _ => traceLogger.LogInformation( message, memberName, sourcePath, sourceLineNumber ),
            };
    }

    /// <summary>
    /// Write a log message using the <see cref="Logger"/>
    /// <see cref="ILogger{TraceLogger}"/>.
    /// </summary>
    /// <remarks>   David, 2022-02-09. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="level">    The level. </param>
    /// <param name="message">  The message. </param>
    /// <returns>   A <see cref="string" />. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0010:Add missing cases", Justification = "<Pending>" )]
    public static string LogEventType<TCategory>( this TraceLogger<TCategory> traceLogger, TraceEventType level, string message )
    {
        switch ( level )
        {
            case TraceEventType.Information:
                traceLogger.Logger?.LogInformationMessage( message );
                break;
            case TraceEventType.Warning:
                traceLogger.Logger?.LogWarningMessage( message );
                break;
            case TraceEventType.Error:
                traceLogger.Logger?.LogErrorMessage( message );
                break;
            case TraceEventType.Critical:
                traceLogger.Logger?.LogCriticalMessage( message );
                break;
            case TraceEventType.Verbose:
                traceLogger.Logger?.LogVerboseMessage( message );
                break;
            default:
                traceLogger.Logger?.LogInformationMessage( message );
                break;
        }
        return message;
    }

    /// <summary>   Write a log message using the <see cref="Logger"/> <see cref="ILogger{TraceLogger}"/>. </summary>
    /// <remarks>   David, 2021-03-13. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="traceLogger">      The traceLogger to act on. </param>
    /// <param name="level">    The level. </param>
    /// <param name="message">  The message. </param>
    public static string LogMessage<TCategory>( this TraceLogger<TCategory> traceLogger, LogLevel level, string message )
    {
        switch ( level )
        {
            case LogLevel.Trace:
                traceLogger.Logger?.LogTraceMessage( message );
                break;
            case LogLevel.Debug:
                traceLogger.Logger?.LogDebugMessage( message );
                break;
            case LogLevel.Information:
                traceLogger.Logger?.LogInformationMessage( message );
                break;
            case LogLevel.Warning:
                traceLogger.Logger?.LogWarningMessage( message );
                break;
            case LogLevel.Error:
                traceLogger.Logger?.LogErrorMessage( message );
                break;
            case LogLevel.Critical:
                traceLogger.Logger?.LogCriticalMessage( message );
                break;
            case LogLevel.None:
                break;
            default:
                traceLogger.Logger?.LogInformationMessage( message );
                break;
        }
        return message;
    }

}
