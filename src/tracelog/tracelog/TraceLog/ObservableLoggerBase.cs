using System.ComponentModel;
using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace cc.isr.Logging.TraceLog;

/// <summary> Defines the contract that must be implemented by View Models
/// capable of logging and tracing. </summary>
/// <remarks>
/// <list type="bullet">View Model Notify implementation: <item>
/// Notify Function calls Synchronization Context Post;</item><item>
/// Async Notify Function calls Synchronization Context Post;</item><item>
/// Sync Notify function calls Synchronization Context Send;</item><item>
/// Raise event (custom implementation) calls Synchronization Context Post;</item><item>
/// Notify is used as the default in property set functions.</item></list> <para>
/// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
/// Licensed under The MIT License.</para><para>
/// David, 2018-12-10. Created from property notifiers
/// </para>
/// </remarks>
/// <remarks>   Specialized constructor for use only by derived class. </remarks>
/// <remarks>   2024-07-29. </remarks>
/// <param name="traceLogger">  The trace logger. </param>
public abstract partial class ObservableLoggerBase( TraceLogger<ObservableLoggerBase> traceLogger ) : INotifyPropertyChanged
{
    #region " construction and cleanup "

    /// <summary>   Gets or sets the trace logger. </summary>
    /// <value> The trace logger. </value>
    public TraceLogger<ObservableLoggerBase> TraceLogger { get; set; } = traceLogger;

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<T>( ref T backingField, T value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<T>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<T>( ref T prop, T value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
        if ( EqualityComparer<T>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<T>( T oldValue, T newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif

        if ( EqualityComparer<T>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " text writer "

    /// <summary>   Adds a display trace event writer. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="textWriter">   The trace Event tWriter. </param>
    public void AddDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
    {
        textWriter.TraceLevel = this.DisplayTraceEventType;
        // deprecated: log level does not map to trace event types, which are used for 
        // filtering the display.  TraceLogger.ToTraceEventType( this.DisplayLogLevel );
        Tracing.TracingPlatform.Instance.AddTraceEventWriter( textWriter );
    }

    /// <summary>   Removes the display text writer described by <paramref name="textWriter"/>. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="textWriter">   The trace Event tWriter. </param>
    public void RemoveDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
    {
        Tracing.TracingPlatform.Instance.RemoveTraceEventWriter( textWriter );
    }

    #endregion

    #region " display trace event type "

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> value name pair for the global trace event
    /// writer. This level determines the level of all the
    /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
    /// lower level.
    /// </summary>
    /// <value> The trace event writer trace event value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<TraceEventType, string> TraceEventWriterTraceEventValueNamePair
    {
        get => this.TraceLogger.TraceEventWriterTraceEventValueNamePair;
        set => _ = this.SetProperty( this.TraceLogger.TraceEventWriterTraceEventValueNamePair, value,
                                     () => this.TraceLogger.TraceEventWriterTraceEventValueNamePair = value );
    }

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> for the global trace event writer. This level
    /// determines the level of all the
    /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
    /// lower level.
    /// </summary>
    /// <value> The type of the trace event writer trace event. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType TraceEventWriterTraceEventType
    {
        get => this.TraceLogger.TraceEventWriterTraceEventType;
        set => _ = this.SetProperty( this.TraceLogger.TraceEventWriterTraceEventType, value,
                                     () => this.TraceLogger.TraceEventWriterTraceEventType = value );
    }

    private KeyValuePair<TraceEventType, string> _displayTraceEventTypeValueNamePair;

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> value name pair for display.
    /// </summary>
    /// <value> The log level value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<TraceEventType, string> DisplayTraceEventTypeValueNamePair
    {
        get => this._displayTraceEventTypeValueNamePair;
        set => _ = this.SetProperty( ref this._displayTraceEventTypeValueNamePair, value );
    }

    private TraceEventType _displayTraceEventType;

    /// <summary>
    /// Gets or sets the <see cref="TraceEventType"/> for display.
    /// </summary>
    /// <value> The trace event writer log level. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType DisplayTraceEventType
    {
        get => this._displayTraceEventType;
        set => _ = this.SetProperty( ref this._displayTraceEventType, value );
    }

    #endregion

    #region " log level "

    /// <summary>
    /// Gets or sets the <see cref="LogLevel"/> value name pair for logging. This level determines
    /// the level of the <see cref="ILogger"/>.
    /// </summary>
    /// <value> The log level value name pair. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public KeyValuePair<LogLevel, string> LogLevelValueNamePair
    {
        get => this.TraceLogger.LoggingLevelValueNamePair;
        set => _ = this.SetProperty( this.TraceLogger.LoggingLevelValueNamePair, value,
                                     () => this.TraceLogger.LoggingLevelValueNamePair = value );
    }

    /// <summary>
    /// Gets or sets the <see cref="LogLevel"/> for logging. This level determines the level of the
    /// <see cref="ILogger"/>.
    /// </summary>
    /// <value> The log level. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public LogLevel LogLevel
    {
        get => this.TraceLogger.MinimumLogLevel;
        set => _ = this.SetProperty( this.TraceLogger.MinimumLogLevel, value,
                                     () => this.TraceLogger.MinimumLogLevel = value );
    }

    #endregion

    #region " log exception "

    /// <summary>   Add exception data. </summary>
    /// <remarks>   David, 2021-07-02. </remarks>
    /// <param name="ex">       The exception. </param>
    /// <returns>   True if exception data was added for this exception; otherwise, false. </returns>
    protected abstract bool AddExceptionData( Exception ex );

    /// <summary>   Log an exception. </summary>
    /// <remarks>   David, 2021-07-03. </remarks>
    /// <param name="ex">               The ex. </param>
    /// <param name="message">          The message. </param>
    /// <param name="memberName">       (Optional) Name of the caller member. </param>
    /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
    /// <returns>   A <see cref="string" />. </returns>
    protected string LogError( Exception ex, string message, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
                                                             [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
                                                             [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        _ = this.AddExceptionData( ex );
        return this.TraceLogger.LogException( message, ex, memberName, sourceFilePath, sourceLineNumber );
    }

    #endregion
}
