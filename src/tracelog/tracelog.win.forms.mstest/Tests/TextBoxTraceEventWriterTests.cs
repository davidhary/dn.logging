using System.Diagnostics;

namespace cc.isr.Logging.TraceLog.WinForms.Tests;

/// <summary>   (Unit Test Class) a text box trace event writer tests. </summary>
/// <remarks>   David, 2020-09-23. </remarks>
[TestClass]
public class TextBoxTraceEventWriterTests
{
    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.Name}";
        try
        {
            TraceListener = new Tracing.TestWriterQueueTraceListener( $"{testContext.FullyQualifiedTestClassName}.TraceListener",
                SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
            Trace.WriteLine( "Initializing", methodFullName );
        }
        catch ( Exception ex )
        {
            Trace.WriteLine( $"Failed initializing the test class: {ex}", methodFullName );

            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {TimeZoneInfo.Local}" );
        Console.WriteLine( $"Testing {typeof( MessagesBox ).Assembly.FullName}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " messages box tests "

    private Tracing.WinForms.TextBoxTraceEventWriter? TextBoxTextWriter { get; set; }

    /// <summary>   (Unit Test Method) should trace message. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ShouldTraceMessage()
    {
        // this is required to create the control handle.
        using Form panel = new();
        using MessagesBox target = new();
        panel.Controls.Add( target );
        // this is required to ensure the controls have handles for consummating the invocation;
        panel.Visible = true;
        bool handleCreated = target.IsHandleCreated;

        this.TextBoxTextWriter = new( target )
        {
            TabCaption = "Log",
            CaptionFormat = "{0} " + Convert.ToChar( 0x1C2 ),
            ResetCount = 1000,
            PresetCount = 500,
            TraceLevel = TraceEventType.Information
        };

        // the tracing listener is already registered.
        // _ = System.Diagnostics.Trace.Listeners.Add( cc.isr.Tracing.TracingPlatform.Instance.TraceEventWriterTraceListener );

        // add the text box text writer.
        Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.TextBoxTextWriter );

        // this can be used for debugging
        // TracingPlatform.Instance.AddTraceEventWriter( TracingPlatform.Instance.CriticalTraceEventTraceListener );
        // Trace.TraceWarning( "testing a warning" );

        int initialTextLength = this.TextBoxTextWriter.TextLength;
        int initialLineCount = this.TextBoxTextWriter.LineCount;

        // send a trace message and see if it gets recorded. 
        string message = $"Message {DateTimeOffset.UtcNow}";

        Trace.TraceInformation( message );

        // Note: This was using Has Unread Messages; but with making the panel visible, 
        // Has Unread Messages could become false if Trace Writer finds the target text box visible.
        // I am not clear why the Tracing tests did not have this issue, but the code was change there as well.

        DateTime endTime = DateTime.Now.AddMilliseconds( 200 );
        while ( this.TextBoxTextWriter.TextLength <= initialTextLength && DateTime.Now < endTime )
        {
            Task.Delay( 1 ).Wait();
            // this is required otherwise the  text Box Trace Event Writer invoke hangs.
            Application.DoEvents();
        }

        Assert.IsTrue( this.TextBoxTextWriter.TextLength > initialTextLength, "Text should be added to the messages box" );

        int expectedLineCount = initialLineCount + 1;
        Assert.AreEqual( expectedLineCount, this.TextBoxTextWriter.LineCount,
            "The number of messages added to this listener should match the expected value " + target.Text );

    }
    #endregion
}
