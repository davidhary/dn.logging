# About

cc.isr.Logging.TraceLog.WinForms is a .Net library supporting Windows Forms controls logging.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.Logging.TraceLog.WinForms is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Logging Repository].

[Logging Repository]: https://bitbucket.org/davidhary/dn.logging

