namespace cc.isr.Logging;

/// <summary>   A console log writer. </summary>
/// <remarks>   2023-03-24. </remarks>
/// <remarks>   Constructor. </remarks>
/// <param name="minimumLogLevel">  The minimum log level. </param>
public class ConsoleLogWriter( LogWriterLevel minimumLogLevel ) : ILogWriter
{
    /// <summary>   Gets or sets the minimum log level. </summary>
    /// <value> The minimum log level. </value>
    private LogWriterLevel MinimumLogLevel { get; set; } = minimumLogLevel;

    /// <summary>   Query if 'level' is enabled. </summary>
    /// <remarks> 
    /// <see href="https://learn.Microsoft.com/en-us/dotnet/core/extensions/custom-logging-provider"/>
    /// public bool IsEnabled(LogLevel logLevel) = _getCurrentConfig().LogLevelToColorMap.ContainsKey(logLevel);
    /// </remarks>
    /// <param name="level">    The level. </param>
    /// 
    /// <returns>   True if enabled, false if not. </returns>
    public bool IsEnabled( LogWriterLevel level )
    {
        return level <= this.MinimumLogLevel;
    }

    /// <summary>   Writes. </summary>
    /// <param name="level">    The level. </param>
    /// <param name="message">  The message. </param>
    public void Write( LogWriterLevel level, string message )
    {
        Console.WriteLine( message );
    }

}
/// <summary>   A logger. </summary>
public static class Logger
{
    /// <summary>   Gets or sets the writer. </summary>
    /// <value> The writer. </value>
    public static ILogWriter Writer { get; set; } = new ConsoleLogWriter( LogWriterLevel.Verbose );
}
