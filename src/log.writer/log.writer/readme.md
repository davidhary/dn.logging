# About

cc.isr.Logging.LogWriter is a .Net library for simple logging 
to use primarily with unit testing libraries.

# How to Use

```
using cc.isr.Logging;

[TestClass]
public class AsyncEventHandlersTests
{

    #region " Fixture construction and cleanup "

    /// <summary>   Initializes the fixture. </summary>
    /// <param name="context">  The context. </param>
    [ClassInitialize]
    public static void InitializeFixture( TestContext context )
    {
        try
        {
            _classTestContext = context;
            Logger.Writer.LogInformation( $"{_classTestContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}" );
        }
        catch ( Exception ex )
        {
            Logger.Writer.LogMemberError( $"Failed initializing fixture:", ex );
            CleanupFixture();
        }
    }
}
```

# Key Features
* outputs log messages to the console.

# Main Types

The main types provided by this library are:

* Logger.Writer Writes logs.

# Feedback

cc.isr.Logging.LogWriter is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Logging Repository].

# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

# [1.0.8484] - 2023-03-25
* Add to the logging solution. 

[1.0.9083]: https://bitbucket.org/davidhary/dn.logging/src/main/
[Logging Repository]: https://bitbucket.org/davidhary/dn.logging

