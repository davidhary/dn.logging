# Serilog Console Application Demonstration

The Logging Tester demonstrates how to enable logging in a .NET Core application.
Console application.

## Dependency Injection
In .NET core, logging is managed via dependency injection. While for ASP.NET 
projects, where injection is automatically created upon starting a new project 
in Startup.cs, a console applications requires a bit of configuration 
to get it up and running.

## Microsoft Extensions Logging
.NET Core has introduced ILogger as a generic interface for logging purposes. 
This interface can be used across different types of applications, such as, 
Console, Asp.Net, Desktop or Form Applications.

In Console applications, which do not have dependency injection by 
default, dependency injection must be explicitly implemented upon starting the 
application.

## Demonstration Design
In Logging Tester, an Application (*App*) class is used to maintain a separation of 
concern between the business-based logic and the logic used to configure and run 
the actual console application. The startup *Program* class bootstraps everything
needed to support the application, which then fires off all the logic that is needed 
to support the "business" needs by way of executing the *Run()* method in the 
*App* class. 

For this demonstration, the business logic is implemented by the *TestService* class.

The dependency injection container provides the means for registering the
individual components used in the program. To this end, a new service called 
*TestService* is registered. 

The *App* class receives an object that meets the *ITestService* interface contract. 
This object is passed-in through the dependency manager. 

Upon startup, the *ServiceCollection* object is configured by adding the dependencies 
to the container collection, which can have a lifetime of Scoped, Transient or Singleton. 
Once the *ServiceCollection* object is configured, an *IServiceProvider* 
(Dependency Management Container) is requested from the ServiceCollection object 
in order to manually resolve the *App* class and kick a logical loop off by calling 
its *Run()* method. 

## Logging

Logging is set up once the Dependency Injection is wired up. More specifically, 
a log to the console is added to visually verify that not only App.Run() method 
is being called but also that the ITestService is being injected and ran from 
the App class. 

This is accomplished by adding new instances of ILoggerFactory for the Console 
(outputs to the console) and Debug (writes log output by way of System.Diagnostics.Debug) 
providers as well as the Serilog provider. These providers have a Singleton lifetime. 
Finally, the logging services are added to the service collection. 

## Running
An instance of *ILogger<Program>* is used to demonstrate direct construction of a logger.
An instance of *ILogger<App>*, which was injected to *App* is used to *LogInfromation*
inside the *App.Run()* method. Additionally, the *ITestService.Run()* of the injected 
service is called to further illustrate that the dependency injection is actually 
managing dependencies correctly. 

Much like *App*, an instance of *ILogger<TestService>*, which was injected into the 
*TestService* class, performs a *LogWarning* inside of *ITestService.Run()* and raises 
an exception to demonstrate the output of an exception with an inner exception.

## Configuration
Logging configuration is accomplished as part for the logging setup in the 
*isr.Logging.Platform*  class.

With Console and Debug loggers, a call to AddOptions() on the *IServiceCollection* 
object could services needed to use the Options pattern inside of the App. In short, 
the Options Pattern allows decoupling feature configuration in the application and 
bind said feature configuration to independent models.

## Key Issue

# Support for .Net 4.72 and 4.8 Framework

Presently (Visual Studio 17.10.4), this project targets .NET 8.0 Core only. Unfortunately, logging fails when targeting legacy .NET frameworks such as 4.72 or 4.8 with the following exception:

```
System.IO.FileLoadException: Could not load file or assembly 'Serilog, Version=2.0.0.0, Culture=neutral, PublicKeyToken=24c2f752a8e58a10' or one of its dependencies. The located assembly's manifest definition does not match the assembly reference. (Exception from HRESULT: 0x80131040)
```

This failure occurred upon attempting to configure the Serilog logging platform form our .NETSTANDARD 2.0 logging library. We are not sure at this time how this could be remedied. Apparently, a similar issue was reported in 2017 ([2017 issue]) that was reported as fixed in Visual Studio 15.5.

# [Serilog Demo] Supports legacy frameworks such as .Net 4.72 and 4.8

The [Serlog Demo] project can be used to target the legacy framworks.

## References
[Serilog]

[Serilog]: https://github.com/serilog/serilog
[2017 issue]: https://developercommunity.visualstudio.com/t/could-not-load-file-or-assembly-error-when-using-s/35539

[Serilog Demo]: https://bitbucket.org/davidhary/dn.logging/src/serilog/serilog.demo
