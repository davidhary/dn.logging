using System.CommandLine;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using cc.isr.Tracing;
using cc.isr.Logging.ILogger;

namespace cc.isr.Logging.Demo;

/// <summary>   A program. </summary>
/// <remarks>   David, 2021-02-04. <para>
/// <see href="https://dev.to/KRUSTY93/net-core-5-0-console-app-configuration-trick-and-tips-c1j"/>
/// </para> </remarks>
internal sealed class Program
{
    private static async Task<int> Main( string[] args )
    {
        RootCommand rootCommand = CreateRootCommand();
        _ = CreateReadCommand( rootCommand );

        return await rootCommand.InvokeAsync( args );
    }

    private static RootCommand CreateRootCommand()
    {
        RootCommand rootCommand = new( "Demonstrate the logging functionality of the cc.isr.Logging.Logger." );
        return rootCommand;
    }

    private static Command CreateReadCommand( RootCommand rootCommand )
    {
#if false
        _ = app.Option( "-d|--dir <value>", "Relative Directory, e.g., ..\\_log", CommandOptionType.SingleValue );
        _ = app.Option( "-l|--level <value>", "Logging level: Information, Warning, Error", CommandOptionType.SingleValue );
        _ = app.Option( "-q|--queue <value>", "Queue Listener: Sync, Async", CommandOptionType.SingleValue );
        _ = app.Option( "-o|--option <value>", "Listener or Provider Logging implementation", CommandOptionType.SingleValue );
#endif
        Option<DirectoryInfo?> dirOption = new(
            name: "--dir",
            description: "The relative folder for the log file, e.g., ..\\_log." );
        dirOption.AddAlias( "-d" );

        Option<LogLevel> levelOption = new(
            name: "--level",
            description: "Logging level: Information, Warning, Error.",
            getDefaultValue: () => LogLevel.Information );
        levelOption.AddAlias( "-l" );

        Option<string> queueListenerMode = new(
            name: "--queue",
            description: "Queue Listener mode: Sync, Async.",
            getDefaultValue: () => "Sync" );
        queueListenerMode.AddAlias( "-q" );

        Option<string> logOption = new(
            name: "--option",
            description: "Listener or Provider Logging implementation: Serilog, none",
            getDefaultValue: () => "Sync" );
        logOption.AddAlias( "-o" );

        Command readCommand = new( "LoggingTester", "Demonstrate the logging functionality of the cc.isr.Logging.Logger." )
            { dirOption,
              levelOption,
              queueListenerMode,
              logOption };
        rootCommand.AddCommand( readCommand );

        readCommand.SetHandler( async ( file, level, consoleColor, logOption ) => await ReadFile( file!, level, consoleColor, logOption ),
            dirOption, levelOption, queueListenerMode, logOption );

        return readCommand;
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
    internal static async Task ReadFile( DirectoryInfo folder, LogLevel level, string queueListenerMode, string logOption )
    {
        switch ( logOption.ToUpperInvariant() )
        {
            case "SERILOG":
                // RunSerilogImplementation( folder, level, queueListenerMode );
                RunSerilogImplementation( queueListenerMode );
                break;

            default:
                break;
        }
        await Task.Delay( 10 );
        Console.Write( "\r\n\r\nDone. Enter key: " );
        _ = Console.ReadKey();
    }

    #region " serilog implementation "

    /// <summary>   Gets or sets the number of expected trace messages. </summary>
    /// <value> The number of expected trace messages. </value>
    internal static int ExpectedTraceMessagesCount { get; set; }

    /// <summary>   Configure logging services. </summary>
    /// <remarks>   David, 2021-02-09. </remarks>
    /// <param name="serviceCollection">    Collection of services. </param>
    private static IServiceCollection ConfigureLoggingServices( string queueListenerMode )
    {
        cc.isr.Logging.ILogger.LoggingPlatform.Instance.RuntimeSerilogLevel = Microsoft.Extensions.Logging.LogLevel.Trace;
        string fileName = ILogger.LoggingPlatform.BuildEntryAssemblySettingsFileName();
        IServiceCollection serviceCollection = cc.isr.Logging.ILogger.LoggingPlatform.ConfigureSerilogServices( fileName );

        if ( string.Equals( queueListenerMode, "Async", StringComparison.OrdinalIgnoreCase ) )
        {
            Console.WriteLine();
            Console.WriteLine( $"Queue option: {queueListenerMode}" );
            Console.WriteLine();
            TracingPlatform.Instance.AddTraceEventWriter( TracingPlatform.Instance.CriticalTraceEventTraceListener );
            // comment out to see all messages.
            // TracingPlatform.Instance.CriticalTraceEventSourceLevel = TracingPlatform.Instance.TraceEventSourceLevel;
        }
        else
        {
            // Add Concurrent Queue trace listener
            TracingPlatform.Instance.AddTraceListener( TracingPlatform.Instance.CriticalTraceEventTraceListener );
        }

        // Add services
        _ = serviceCollection.AddTransient<ITestService, TestService>();

        // Add the application
        return serviceCollection.AddTransient<App>();

    }

    /// <summary>   Executes the 'provider implementation' operation. </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    private static void RunSerilogImplementation( string queueListenerMode )
    {
        // Create service collection
        IServiceCollection serviceCollection = ConfigureLoggingServices( queueListenerMode );

        // Create service provider
        ILogger.LoggingPlatform.Instance.LoggingServicesProvider = serviceCollection.BuildServiceProvider();

        // a logger can be instantiated at this point. 
        ILogger<Program> logger = ILogger.LoggingPlatform.Instance.CreateLogger<Program>()!;

        logger.LogInformation( "Starting application..." );

        // count expected trace messages based on the listener level.
        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
            Program.ExpectedTraceMessagesCount += 1;

        // entry to inject the logger and run the application
        serviceCollection.BuildServiceProvider().GetService<App>()!.Run();
    }

    /// <summary>   Configure logging services. </summary>
    /// <remarks>   David, 2021-02-09. </remarks>
    /// <param name="serviceCollection">    Collection of services. </param>
    private static void ConfigureLoggingServices( IServiceCollection serviceCollection,
                                                  DirectoryInfo folder, LogLevel level, string queueListenerMode )
    {
#if false
        // Shows how to set up the Logging rather than the Serilog sinks.
        cc.isr.Logging.ILogger.Platform.Get().ConfigureLoggingServices( serviceCollection,
                                                                          Logging.ILogger.LoggingProviderKinds.Serilog
                                                                          | Logging.ILogger.LoggingProviderKinds.ConsoleLog
                                                                          | Logging.ILogger.LoggingProviderKinds.DebugLog
                                                                          | Logging.ILogger.LoggingProviderKinds.TraceLog );
#endif

        cc.isr.Logging.ILogger.LoggingPlatform.Instance.RuntimeSerilogLevel = Microsoft.Extensions.Logging.LogLevel.Trace;
        string fileName = ILogger.LoggingPlatform.BuildEntryAssemblySettingsFileName();


        cc.isr.Logging.ILogger.LoggingPlatform.Instance.ConfigureLoggingServices( fileName, serviceCollection,
                                                                                  Logging.ILogger.LoggingProviderKinds.Serilog | LoggingProviderKinds.ConsoleLog
                                                                                  | LoggingProviderKinds.DebugLog | LoggingProviderKinds.TraceLog );

        if ( string.Equals( queueListenerMode, "Async", StringComparison.OrdinalIgnoreCase ) )
        {
            Console.WriteLine();
            Console.WriteLine( $"Queue option: {queueListenerMode}" );
            Console.WriteLine();
            TracingPlatform.Instance.AddTraceEventWriter( TracingPlatform.Instance.CriticalTraceEventTraceListener );
            // comment out to see all messages.
            // TracingPlatform.Instance.CriticalTraceEventSourceLevel = TracingPlatform.Instance.TraceEventSourceLevel;
        }
        else
        {
            // Add Concurrent Queue trace listener
            TracingPlatform.Instance.AddTraceListener( TracingPlatform.Instance.CriticalTraceEventTraceListener );
        }

        // Add services
        _ = serviceCollection.AddTransient<ITestService, TestService>();

        // Add the application
        _ = serviceCollection.AddTransient<App>();
    }

    /// <summary>   Executes the 'provider implementation' operation. </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
    private static void RunSerilogImplementation( DirectoryInfo folder, LogLevel level, string queueListenerMode )
    {
        // Create service collection
        ServiceCollection serviceCollection = new();
        ConfigureLoggingServices( serviceCollection, folder, level, queueListenerMode );

        // a logger can be instantiated at this point. 
        ILogger<Program> logger = ILogger.LoggingPlatform.Instance.CreateLogger<Program>()!;

        logger.LogInformation( "Starting application..." );

        // count expected trace messages based on the listener level.
        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
            Program.ExpectedTraceMessagesCount += 1;

        // entry to inject the logger and run the application
        serviceCollection.BuildServiceProvider().GetService<App>()!.Run();
    }

    #endregion
}

#region " logger-injected application classes "

/// <summary>
/// To maintain a separation of concern between our business based logic and the logic we use to
/// configure and run the actual console application, let's create a new class called App.cs. The
/// goal being that we will use Program.cs to bootstrap everything need to support our
/// application and then fire off all the logic that is needed to support out "business" needs by
/// way of executing the Run() method in App.cs.
/// </summary>
/// <remarks>   David, 2021-02-04. </remarks>
public sealed class App( ITestService testService, ILogger<App> logger )
{
    private readonly ITestService _testService = testService;
    private readonly ILogger<App> _logger = logger;

    public void Run()
    {
        this._logger.LogInformation( "Running application." );

        // count expected trace messages based on the listener level.

        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
            Program.ExpectedTraceMessagesCount += 1;

        TimeSpan timeout = TimeSpan.FromMilliseconds( 100 );
        bool fileOpened = HeaderWriter.AwaitFullFileName( timeout, TimeSpan.FromMilliseconds( 20 ), TimeSpan.FromMilliseconds( 20 ) );
        string message = fileOpened
            ? $"Logging into {HeaderWriter.FullFileName}."
            : $"File not open after {timeout.TotalMilliseconds}ms.";
        this._logger.LogInformation( $"{message}" );

        // count expected trace messages based on the listener level.

        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
            Program.ExpectedTraceMessagesCount += 1;

        try
        {
            this._testService.Run();
        }
        catch ( Exception ex )
        {
            ex.Data.Add( $"data {ex.Data.Count}", "internal exception data" );
            ex.Data.Add( $"data {ex.Data.Count}", "internal exception data 2" );
            InvalidOperationException reportEx = new( $"Caught in {nameof( App )}", ex );
            reportEx.Data.Add( $"data {reportEx.Data.Count}", "parent exception data" );
            reportEx.Data.Add( $"data {reportEx.Data.Count}", "parent exception data 2" );
            this._logger.LogWarning( "warning; exception" );
            this._logger.LogError( reportEx, "reporting exception" );
            if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
                Program.ExpectedTraceMessagesCount += 1;
        }

        Console.WriteLine();
        Console.WriteLine( "--- Issue 184: Debug and trace events are filtered in even under warnings level." );
        Console.WriteLine( $"--- Queue Trace Listener Records (expecting {Program.ExpectedTraceMessagesCount} or {Program.ExpectedTraceMessagesCount + 1}):" );
        // Console write line missed the CRLF preceding the stack trace line.
        string formatted = TracingPlatform.Instance.CriticalTraceEventTraceListener.DequeueAllMessages();
        Console.WriteLine( formatted );
        Console.WriteLine( "--- End of Queue Trace Listener Records" );
        Console.WriteLine();

        // report number of elements in the Queue Trace Listener
        Console.WriteLine( $"Queue Trace Listener includes {TracingPlatform.Instance.CriticalTraceEventTraceListener.Queue.Count} records (expecting {Program.ExpectedTraceMessagesCount} or {Program.ExpectedTraceMessagesCount + 1})." );
        Console.WriteLine( $"Flushing the Queue Trace Listener ." );
        TracingPlatform.Instance.CriticalTraceEventTraceListener.Flush();
        Console.WriteLine( $"Queue Trace Listener includes {TracingPlatform.Instance.CriticalTraceEventTraceListener.Queue.Count} records after flush." );

        Console.WriteLine();
        Console.WriteLine( "--- Queue Trace Listener Records after flush:" );
        // Console write line missed the CRLF preceding the stack trace line.
        formatted = TracingPlatform.Instance.CriticalTraceEventTraceListener.DequeueAllMessages();
        Console.WriteLine( formatted );
        Console.WriteLine( "--- End of Queue Trace Listener Records" );
        Console.WriteLine();


        Log.CloseAndFlush();

    }
}
/// <summary>   Interface for test service. </summary>
/// <remarks>   David, 2021-02-04. </remarks>
public interface ITestService
{
    void Run();
}
/// <summary>   A service for accessing tests information. </summary>
/// <remarks>   David, 2021-02-04. </remarks>
internal sealed class TestService( ILogger<TestService> logger ) : ITestService
{
    private readonly ILogger<TestService> _logger = logger;

    /// <summary>   Runs this object. </summary>
    /// <remarks>   2024-07-25. </remarks>
    /// <exception cref="DivideByZeroException">    Thrown when an attempt is made to divide a number
    ///                                             by zero. </exception>
    public void Run()
    {
        this._logger.LogDebug( $"Running logging service." );
        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Verbose )
            Program.ExpectedTraceMessagesCount += 1;

        this._logger.LogTrace( $"Throwing divide by zero exception." );
        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Verbose )
            Program.ExpectedTraceMessagesCount += 1;

        throw new DivideByZeroException();
    }
}

#endregion
