### ISR Core Logging Library

The ISR Logging library encapsulates access to the [Serilog] framework for 
logging in .NET 5.0 using the ILogger interface.

## ILogger Configuration using the Platform Class 

### Dependency Injection
In .NET core, logging is managed via dependency injection. While for ASP.NET 
projects, where injection is automatically created upon starting a new project 
in Startup.cs, a console applications requires a bit of configuration 
to get it up and running.

### Microsoft Extensions Logging
.NET Core has introduced ILogger as a generic interface for logging purposes. 
This interface can be used across different types of applications, such as, 
Console, Asp.Net, Desktop or Form Applications.

### Logging Setup
Logging is set up in the *Platform* class by adding the logging services to the 
container *ServiceCollection*. This is accomplished by adding new instances 
of *ILoggerFactory* for the Console (outputs to the console), Debug (writes log 
output by way of System.Diagnostics.Debug), as well as the Serilog provider. 
These providers have a Singleton lifetime. 

Once the *ServiceCollection* object is configured, an *IServiceProvider* 
(Dependency Management Container) is requested from the ServiceCollection object.

The *Platform* can then create *ILogger* instances for each logging *Category*, 
which is, basically, a type that is set to log. For example, the *LoggingTester*
demo, creates an instance of *ILogger<Program>*.

### Configuration
Configuration is accomplished through a JSON file. 

A *ConfigurationBuilder* object is constructed to:
* Define the absolute path of the directory where the *appsettings.json* file lives.  
* Add the *appsettings.json* file to the *IConfigurationRoot* object, denoting it as not optional.
* Build configuration and return an *IConfigurationRoot* object.

Thereafter, the [Serilog] Logger is created and configured to read the configuration 
from the *IConfigurationRoot* object.

## Serilog
[Serilog] is a newer logging framework for .NET. It was built with structured logging 
in mind. It makes it easy to record custom object properties and even output logs to 
numerous destinations called 'Sinks'.

### Sinks
Sinks are used to direct logs to destinations, such as file, debug or console. 

Note: With the debug sink, log statements are added to the Visual Studio Debug window.  

Serilog’s sinks can be configured in code:

```
using (var log = new LoggerConfiguration()  
    .WriteTo.Console()  
    .CreateLogger())  
{
    log.Information("Hello, Serilog!");  
    log.Warning("Goodbye, Serilog.")  
}
```

### Serilog’s internal debug logging
Internal problems with Serilog, can be logged by subscribing to it’s internal 
events and writing them to the debug window or a console:
```
Serilog.Debugging.SelfLog.Enable(message => Debug.WriteLine(message));
Serilog.Debugging.SelfLog.Enable(Console.Error);
```
Note: Internal logging will not write to any user-defined sinks.

### Log levels and filtering
Log levels such as verbose, debug, information, warning, error, and fatal logging 
levels are valuable for specifying when only certain levels need to be logged to 
specific logging sinks and to reduce logging in production.

Logs can then be searched by the logging level.

### Structured logging

[Serilog] was designed to log variables in code:

```
Log.Debug("Processing item {ItemNumber} of {ItemCount}", itemNumber, itemCount);
```
With Serilog these variables can be recorded as JSON or sent to a [log management] 
solution like [Retrace].

### Sending alerts for exceptions

If you want to send alerts when a new exception occurs, send your exceptions to an 
[error reporting] solution, like [Retrace], that is designed for this. 
[Retrace] can filter duplicate your errors so you can figure out when an error is 
truly new or regressed. You can also track its history, error rates, and a bunch of 
other cool things.

### Use filters to suppress certain logging statements

Serilog has the ability to specify log levels to send to specific sinks or 
suppress all log messages. Use the *Restricted To Minimum Level* parameter.
```
Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .WriteTo.File("log.txt")
    .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
    .CreateLogger();
```

### Make your own custom sinks

If you want to do something that the standard Serilog sinks do not support, you can 
search online for one or write your own. One example could be a target for writing 
to Azure Storage.  

As an example of a custom target, you can review the [source code for our Serilog sink]
for sending logs to [Retrace].

### Customize the output format of your Logs

With Serilog you can control the format of your logs, such as which fields you 
include, their order, and etc.

Here is a simple example:
```
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console(outputTemplate:
        "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
    .CreateLogger();
```
The following fields can be used in a custom output template:
```
    Exception
    Level
    Message
    NewLine
    Properties
    Timestamp
```
If that isn’t enough, you can implement ITextFormatter to have even more control of
the output format.  

Serilog has also great support from writing your log files as JSON. It has a built-in 
JSON formatter that you can use.
```
Log.Logger = new LoggerConfiguration()
    .WriteTo.File(new CompactJsonFormatter(), "log.txt")
    .CreateLogger();
```

#### File name date and logging timestamp
Because, presently, [Serilog.Sinks.File] uses local time for setting the rolling file name by date,
the timestamp used inside this file should also be set to local time. For completeness,
the timestamp also includes the local time offset from universal time. 


### Enrich your logs with more context

Most logging frameworks provide some way to log additional context values across
all logging statements. This is perfect for setting something like a UserId at the
beginning for a request and having it included in every log statement.  

Serilog implements this by what they call enrichment.

Below is a simple example of how to add the current thread id to the logging data captured.
```
var log = new LoggerConfiguration()
    .Enrich.WithThreadId()
    .WriteTo.Console()
    .CreateLogger();
```
To really use enrichment with your own app, you will want to use the LogContext.
```
var log = new LoggerConfiguration()
    .Enrich.FromLogContext()
```
After configuring the log context, you can then use it to push properties into the
context of the request. Here is an example:
```
log.Information("No contextual properties");
using (LogContext.PushProperty("A", 1))
{
    log.Information("Carries property A = 1");
    using (LogContext.PushProperty("A", 2))
    using (LogContext.PushProperty("B", 1))
    {
        log.Information("Carries A = 2 and B = 1");
    }
    log.Information("Carries property A = 1, again");
}
```

### Json Configuration
See [Tech Repository] and [serilog settings configuration] and [C-Sharp Corner]

A file name can be defined as Rolling:
```
    "WriteTo": [
      { "Name": "Console" },
      {
        "Name": "File",
        "Args": {
          "path": "../_Logs/ex_.log",
          "outputTemplate": "{Timestamp:o} [{Level:u3}] ({SourceContext}) {Message}{NewLine}{Exception}",
          "rollingInterval": "Day",
          "retainedFileCountLimit": 7
        }
      }
    ],
```

In XML and JSON configuration formats, environment variables can be used in setting values. This means, for instance, that the log file path 
can be based on TMP or APPDATA:
```
value="%APPDATA%\MyApp\log.txt" />
"path": "%APPDATA%/MyApp/_Logs/ex_.log",
```

### Getting the active file name
The file name is updated upon writing the header and can be had from the Header Writer class. 

### Enriched Exception Details
This project uses a customized version of [Serilog.Enrichers.Demystify] to emit an enriched exception information.

## References
[bitscry]  
[Chad Ramos]
[Stackify]

### Feedback

cc.isr.Logging is released as open source under the MIT license. Bug reports and contributions are welcome at the [Logging Repository].

[Logging Repository]: https://bitbucket.org/davidhary/dn.logging

[bitscry]: https://blog.bitscry.com/2017/05/31/logging-in-net-core-console-applications//
[Chad Ramos]: https://pioneercode.com/post/dependency-injection-logging-and-configuration-in-a-dot-net-core-console-app
[error reporting]: https://stackify.com/error-reporting/
[log management]: https://stackify.com/retrace-log-management/
[Retrace]: https://stackify.com/retrace/
[Serilog]: https://github.com/serilog
[Serilog settings configuration]: https://github.com/serilog/serilog-settings-configuration
[Stackify]: https://stackify.com/serilog-tutorial-net-logging/
[Structured Logging]: https://stackify.com/what-is-structured-logging-and-why-developers-need-it/
[source code for our Serilog sink]: https://github.com/stackify/Serilog.Sinks.Stackify/blob/master/Serilog.Sinks.Stackify/Sinks/Stackify/StackifySink.cs
[Tech Repository]: https://www.techrepository.in/blog/posts/writing-logs-to-different-files-serilog-asp-net-core
[C-Sharp Corner]: https://www.c-sharpcorner.com/article/serilog-in-dotnet-core/
[Issue #154]: https://github.com/serilog/serilog-sinks-file/issues/154
[Serilog]: https://github.com/serilog/serilog
[Serilog.Enrichers.Demystify]: https://github.com/nblumhardt/serilog-enrichers-demystify
[Serilog.Sinks.File]: https://github.com/serilog/serilog-sinks-file
[Serilog.Sinks.File.Header]: https://github.com/cocowalla/serilog-sinks-file-header
