
// [assembly: System.Reflection.AssemblyDescription( "Core Logging Library" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "cc.isr.Logging.MSTest,PublicKey=" + cc.isr.Logging.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "cc.isr.Logging.Demo,PublicKey=" + cc.isr.Logging.SolutionInfo.PublicKey )]
