using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace cc.isr.Logging.ILogger;

/// <summary>   Provides the logging platform. </summary>
/// <remarks>   David, 2021-02-08. </remarks>
[Obsolete( "Fails targeting .NET Frameworks" )]
public class LoggingPlatform : IDisposable
{
    #region " construction and cleanup "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    private LoggingPlatform()
    {
        this.SerilogLevelSwitch = new Serilog.Core.LoggingLevelSwitch( Serilog.Events.LogEventLevel.Debug );
        this.TraceSourceSwitch = new System.Diagnostics.SourceSwitch( LoggingPlatform.TRACE_SOURCE_SWITCH_NAME )
        {
            Level = System.Diagnostics.SourceLevels.Information
        };
    }

    #region " disposable support "

    /// <summary> Gets the disposed indicator. </summary>
    /// <value> The disposed indicator. </value>
    public bool IsDisposed { get; private set; }

    /// <summary>
    /// Releases the unmanaged resources used by the class.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="disposing"> True to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected virtual void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.LoggingServicesProvider?.Dispose();
            }
        }
        finally
        {
            this.IsDisposed = true;
        }
    }

    /// <summary> Calls <see cref="Dispose(bool)" /> to cleanup. </summary>
    /// <remarks>
    /// Do not make this method Overridable (virtual) because a derived class should not be able to
    /// override this method.
    /// </remarks>
    public void Dispose()
    {
        this.Dispose( true );
        // Take this object off the finalization(Queue) and prevent finalization code 
        // from executing a second time.
        GC.SuppressFinalize( this );
    }

    /// <summary>
    /// This destructor will run only if the Dispose method does not get called. It gives the base
    /// class the opportunity to finalize. Do not provide destructors in types derived from this
    /// class.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    ~LoggingPlatform()
    {
        // Do not re-create Dispose clean-up code here.
        // Calling Dispose(false) is optimal for readability and maintainability.
        this.Dispose( false );
    }

    #endregion

    #endregion

    #region " lazy singleton "

    /// <summary> The singleton instance </summary>
    private static readonly LoggingPlatform? _instance = new();

    /// <summary> Instantiates the default instance of the class. </summary>
    /// <remarks> Use this property to instantiate a single instance of this class. This class uses
    /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved or if is was disposed. </remarks>
    /// <returns> A new or existing instance of the class. </returns>
    public static LoggingPlatform Instance => _instance!;

    #endregion

    #region " trace switch "

    /// <summary>   Name of the trace source switch. </summary>
    public const string TRACE_SOURCE_SWITCH_NAME = "TraceSourceSwitch";

    /// <summary>
    /// Gets the Trace source switch, which controls the trace source if using Microsoft rather than
    /// Serilog tracing.
    /// </summary>
    /// <value> The trace source switch. </value>
    public System.Diagnostics.SourceSwitch TraceSourceSwitch { get; }

    /// <summary>   Gets or sets the source level above which messages are ignored. </summary>
    /// <value> The source level above which messages are ignored. </value>
    public System.Diagnostics.SourceLevels RuntimeTraceLevel
    {
        get => this.TraceSourceSwitch.Level;
        set
        {
            if ( value != this.RuntimeTraceLevel )
            {
                this.TraceSourceSwitch.Level = value;
            }
        }
    }

    #endregion

    #region " serilog log level switch "

    /// <summary>   Gets the Serilog level switch. </summary>
    /// <value> The Serilog level switch. </value>
    public Serilog.Core.LoggingLevelSwitch SerilogLevelSwitch { get; }

    /// <summary>   Converts a <see cref="LogLevel"/> level to
    /// <see cref="Serilog.Events.LogEventLevel"/>. </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    /// <param name="level">    The level. </param>
    /// <returns>   Level as a Serilog.Events.LogEventLevel. </returns>
    public static Serilog.Events.LogEventLevel ToSerilogEventLevel( LogLevel level )
    {
        return level switch
        {
            Microsoft.Extensions.Logging.LogLevel.Critical => Serilog.Events.LogEventLevel.Fatal,
            Microsoft.Extensions.Logging.LogLevel.Debug => Serilog.Events.LogEventLevel.Debug,
            Microsoft.Extensions.Logging.LogLevel.Error => Serilog.Events.LogEventLevel.Error,
            Microsoft.Extensions.Logging.LogLevel.Information => Serilog.Events.LogEventLevel.Information,
            Microsoft.Extensions.Logging.LogLevel.Trace => Serilog.Events.LogEventLevel.Verbose,
            Microsoft.Extensions.Logging.LogLevel.Warning => Serilog.Events.LogEventLevel.Warning,
            Microsoft.Extensions.Logging.LogLevel.None => Serilog.Events.LogEventLevel.Information,
            _ => Serilog.Events.LogEventLevel.Information,
        };
    }

    /// <summary>   Converts a level to a log level. </summary>
    /// <remarks>   David, 2021-08-13. </remarks>
    /// <param name="level">    The level. </param>
    /// <returns>   Level as a Serilog.Events.LogEventLevel. </returns>
    public static LogLevel ToLogLevel( Serilog.Events.LogEventLevel level )
    {
        return level switch
        {
            Serilog.Events.LogEventLevel.Verbose => Microsoft.Extensions.Logging.LogLevel.Trace,
            Serilog.Events.LogEventLevel.Debug => Microsoft.Extensions.Logging.LogLevel.Debug,
            Serilog.Events.LogEventLevel.Information => Microsoft.Extensions.Logging.LogLevel.Information,
            Serilog.Events.LogEventLevel.Warning => Microsoft.Extensions.Logging.LogLevel.Warning,
            Serilog.Events.LogEventLevel.Error => Microsoft.Extensions.Logging.LogLevel.Error,
            Serilog.Events.LogEventLevel.Fatal => Microsoft.Extensions.Logging.LogLevel.Critical,
            _ => Microsoft.Extensions.Logging.LogLevel.Information,
        };
    }

    /// <summary>
    /// Gets or sets the runtime Serilog minimum level Threshold.
    /// </summary>
    /// <value> The log level. </value>
    public LogLevel RuntimeSerilogLevel
    {
        get => LoggingPlatform.ToLogLevel( this.SerilogLevelSwitch.MinimumLevel );
        set
        {
            if ( value != this.RuntimeSerilogLevel )
            {
                this.SerilogLevelSwitch.MinimumLevel = ToSerilogEventLevel( value );
            }
        }
    }

    #endregion

    #region " settings file name "

    /// <summary>
    /// Builds application settings file title and extension consisting of the
    /// <paramref name="assemblyName"/>+<paramref name="suffix"/>+.json.
    /// </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="assemblyName"> The assembly name. </param>
    /// <param name="suffix">       (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A file title and extension. </returns>
    public static string BuildAppSettingsFileName( string assemblyName, string suffix = ".Serilog" )
    {
        return $"{assemblyName}{suffix}.json";
    }

    /// <summary>
    /// Builds application settings file name consisting of the <para>
    /// 'Assembly Name'+<paramref name="suffix"/>+.json.</para>
    /// </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="assembly"> The assembly, which could be the calling, executing or entry assembly. </param>
    /// <param name="suffix">   (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildAppSettingsFileName( System.Reflection.Assembly assembly, string suffix = ".Serilog" )
    {
        return LoggingPlatform.BuildAppSettingsFileName( assembly.GetName().Name, suffix );
    }

    /// <summary>   Builds calling assembly settings file name. </summary>
    /// <remarks>   David, 2021-07-01. </remarks>
    /// <param name="suffix">   (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildCallingAssemblySettingsFileName( string suffix = ".Serilog" )
    {
        return LoggingPlatform.BuildAppSettingsFileName( System.Reflection.Assembly.GetCallingAssembly().GetName().Name, suffix );
    }

    /// <summary>   Builds entry assembly log settings file name. </summary>
    /// <remarks>   David, 2021-07-01. </remarks>
    /// <param name="suffix">   (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A file title and extension. </returns>
    public static string BuildEntryAssemblySettingsFileName( string suffix = ".Serilog" )
    {
        // the entry assembly might be null when called from the test platform, 
        // because the entry assembly must be a managed type, which the test platform might not.
        System.Reflection.Assembly assembly = System.Reflection.Assembly.GetEntryAssembly() is null
                                                    ? System.Reflection.Assembly.GetCallingAssembly()
                                                    : System.Reflection.Assembly.GetEntryAssembly();
        return LoggingPlatform.BuildAppSettingsFileName( assembly.GetName().Name, suffix );
    }

    /// <summary>
    /// Builds the settings file name of the loaded assembly in which the <paramref name="type"/> is
    /// defined.
    /// </summary>
    /// <remarks>   David, 2021-07-01. </remarks>
    /// <param name="type">     The type which assembly is selected. </param>
    /// <param name="suffix">   (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildAssemblySettingsFileName( Type type, string suffix = ".Serilog" )
    {
        return LoggingPlatform.BuildAppSettingsFileName( System.Reflection.Assembly.GetAssembly( type ).GetName().Name, suffix );
    }

    #endregion

    #region " configure logger "

    /// <summary>   Gets or sets the logging services provider. </summary>
    /// <value> The service provider. </value>
    public ServiceProvider? LoggingServicesProvider { get; set; }

    /// <summary>
    /// Create a service collection and configure and add Serilog services to this collection.
    /// </summary>
    /// <remarks>   2024-07-24. </remarks>
    /// <param name="settingsFileName"> Filename of the settings file. </param>
    /// <returns>   An IServiceCollection. </returns>
    public static IServiceCollection ConfigureSerilogServices( string settingsFileName )
    {
        // check file existence

        FileInfo fi = new( System.IO.Path.Combine( AppContext.BaseDirectory, settingsFileName ) );
        if ( !fi.Exists )
            throw new FileNotFoundException( $"Logging configuration file '{fi.FullName}' not found using '{AppContext.BaseDirectory}' folder and '{settingsFileName}' file name.", fi.FullName );

        // construct the collection of services.

        Microsoft.Extensions.DependencyInjection.IServiceCollection serviceCollection = new ServiceCollection();

        // Add logging services.

        _ = serviceCollection.AddLogging( loggingBuilder =>
        {
            _ = loggingBuilder.AddSerilog();
            _ = loggingBuilder.AddConsole();
            _ = loggingBuilder.AddDebug();
        } );

        // Build the configuration

        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath( System.AppContext.BaseDirectory )
            .AddJsonFile( settingsFileName, false )
            .Build();

        // Initialize the Serilog logger using Json configuration file.
        Serilog.Log.Logger = new Serilog.LoggerConfiguration()
             .ReadFrom.Configuration( configuration )
             .CreateLogger();

        // Add access to generic IConfigurationRoot
        return serviceCollection.AddSingleton( configuration );

    }

    /// <summary>   Configure logging services. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="settingsFileName">     Filename of the settings file. </param>
    /// <param name="serviceCollection">    Collection of services. </param>
    /// <param name="providerKinds">        The provider kinds. </param>
    public void ConfigureLoggingServices( string settingsFileName, IServiceCollection serviceCollection, LoggingProviderKinds providerKinds )
    {
        // check file existence
        FileInfo fi = new( System.IO.Path.Combine( AppContext.BaseDirectory, settingsFileName ) );
        if ( !fi.Exists )
            throw new FileNotFoundException( $"Logging configuration file '{fi.FullName}' not found using '{AppContext.BaseDirectory}' folder and '{settingsFileName}' file name.", fi.FullName );

        // Build configuration
        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath( AppContext.BaseDirectory )
            .AddJsonFile( settingsFileName, false, true )
            .Build();

        // Add logging
        _ = serviceCollection.AddLogging( loggingBuilder =>
        {
            if ( LoggingProviderKinds.Serilog == (LoggingProviderKinds.Serilog & providerKinds) )
                _ = loggingBuilder.AddSerilog();
            if ( LoggingProviderKinds.ConsoleLog == (LoggingProviderKinds.ConsoleLog & providerKinds) )
                _ = loggingBuilder.AddConsole();
            if ( LoggingProviderKinds.DebugLog == (LoggingProviderKinds.DebugLog & providerKinds) )
                _ = loggingBuilder.AddDebug();
            if ( LoggingProviderKinds.TraceLog == (LoggingProviderKinds.TraceLog & providerKinds) )
                _ = loggingBuilder.AddTraceSource( this.TraceSourceSwitch );
            _ = loggingBuilder.AddConfiguration( configuration );
        } );

        // Initialize the Serilog logger using Json configuration file.
        Serilog.Log.Logger = new LoggerConfiguration()
             .ReadFrom.Configuration( configuration )
             // .Enrich.WithExceptionDetails()
             // .Enrich.With<ExceptionDataEnricher>()
             // .WriteTo.File(new JsonFormatter( renderMessage: true ), @"..\_log\log-{Date}.txt" )
             .Enrich.WithDemystifiedStackTraces()
             // .Enrich.WithExceptionDetails( new DestructingOptionsBuilder().WithIgnoreStackTraceAndTargetSiteExceptionFilter() )
             .MinimumLevel.ControlledBy( this.SerilogLevelSwitch )
             // .MinimumLevel.Verbose()
             .CreateLogger();

        // enable self log to log internal Serilog exceptions
        Serilog.Debugging.SelfLog.Enable( Console.Error );
        Serilog.Debugging.SelfLog.Enable( message => System.Diagnostics.Debug.WriteLine( message ) );

        // Add access to generic IConfigurationRoot
        _ = serviceCollection.AddSingleton( configuration );

        // Create service provider
        this.LoggingServicesProvider = serviceCollection.BuildServiceProvider();

    }

    /// <summary>   Reset <see cref="Log.Logger"/> to default and dispose the original if possible.. </summary>
    /// <remarks>   David, 2021-03-13. </remarks>
    public static void CloseAndFlush()
    {
        Serilog.Log.CloseAndFlush();
    }

    /// <summary>   Configure logging services. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <param name="settingsFileName"> Filename of the settings file. </param>
    /// <param name="providerKinds">    The provider kinds. </param>
    internal void ConfigureLoggingServices( string settingsFileName, LoggingProviderKinds providerKinds )
    {
        // Create service collection
        ServiceCollection serviceCollection = new();

        this.ConfigureLoggingServices( settingsFileName, serviceCollection, providerKinds );
    }

    #endregion

    #region " logger constructions "

    /// <summary>   Creates a Serilog logger. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="settingsFileName"> Filename of the settings file. </param>
    /// <returns>   The new Serilog logger. </returns>
    public ILogger<TCategory>? CreateSerilogLogger<TCategory>( string settingsFileName )
    {
        this.ConfigureLoggingServices( settingsFileName, Logging.ILogger.LoggingProviderKinds.Serilog );
        return this.LoggingServicesProvider?.GetService<ILoggerFactory>()?.CreateLogger<TCategory>();
    }

    #endregion

    #region " internal logger constructions "

    /// <summary>   Creates the logger. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <returns>   The new logger. </returns>
    public ILogger<TCategory>? CreateLogger<TCategory>()
    {
        return this.LoggingServicesProvider?.GetService<ILoggerFactory>()?.CreateLogger<TCategory>();
    }

    /// <summary>   Creates the logger. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="settingsFileName"> Filename of the settings file. </param>
    /// <param name="providerKinds">    The provider kinds. </param>
    /// <returns>   The new logger. </returns>
    internal ILogger<TCategory>? CreateLogger<TCategory>( string settingsFileName, LoggingProviderKinds providerKinds )
    {
        this.ConfigureLoggingServices( settingsFileName, providerKinds );
        return this.LoggingServicesProvider?.GetService<ILoggerFactory>()?.CreateLogger<TCategory>();
    }

    /// <summary>   Gets the logger. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <returns>   The logger. </returns>
    internal ILogger<TCategory>? GetLogger<TCategory>()
    {
        return this.LoggingServicesProvider?.GetService<ILogger<TCategory>>();
    }

    #endregion

    #region " logger singleton instance "

    private static string? _logSettingsFileName;

    /// <summary>
    /// Gets or sets the filename of the log settings file, which defaults to the Entry assembly name.
    /// </summary>
    /// <value> The filename of the log settings file. </value>
    public static string LogSettingsFileName
    {
        get
        {
            if ( string.IsNullOrWhiteSpace( LoggingPlatform._logSettingsFileName ) )
                _logSettingsFileName = cc.isr.Logging.ILogger.LoggingPlatform.BuildEntryAssemblySettingsFileName();
            return _logSettingsFileName!;
        }
        set => _logSettingsFileName = value;
    }

    #endregion
}
/// <summary>   A bit-field of flags for specifying the kinds of logging provider. </summary>
/// <remarks>   David, 2021-02-08. </remarks>
[Flags]
public enum LoggingProviderKinds
{
    /// <summary>   A binary constant representing the none flag. </summary>
    None = 0,
    /// <summary>   A binary constant representing the console log provider. </summary>
    ConsoleLog = 1,
    /// <summary>   A binary constant representing the debug log provider. </summary>
    DebugLog = 2,
    /// <summary>   A binary constant representing the trace log provider </summary>
    TraceLog = 4,
    /// <summary>   A binary constant representing the Serilog provider. </summary>
    Serilog = 8
}


