
using System;
using System.Linq;
using System.CommandLine;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog.Debugging;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Reflection;
using System.Runtime.Versioning;
using Serilog;
using cc.isr.Logging.Orlog;
using cc.isr.Tracing;

namespace Orlog.Demo;

/// <summary>   A program. </summary>
/// <remarks>   David, 2021-02-04. </remarks>
internal sealed partial class Program
{
    /// <summary>   Main entry-point for this application. </summary>
    /// <remarks>   2023-04-18. </remarks>
    /// <param name="args"> An array of command-line argument strings. </param>
    /// <returns>   Exit-code for the process - 0 for success, else an error code. </returns>
    private static async Task<int> Main( string[] args )
    {
        RootCommand rootCommand = Program.CreateRootCommand();
        Program.DefineCommandLIneOptions( rootCommand );
        Program.DefineExecution( rootCommand );
        return await rootCommand.InvokeAsync( args )
            .ContinueWith( failedTask => OnThreadException( new ThreadExceptionEventArgs( failedTask.Exception ) ) ); ;
    }

    private static int OnThreadException( ThreadExceptionEventArgs args )
    {
        if ( args == null || args.Exception is null ) return 0;
        Console.WriteLine( $"Exception; {Environment.NewLine}{args.Exception}." );
        return -1;
    }

    /// <summary>   Define execution. </summary>
    /// <remarks>   2023-04-18. </remarks>
    /// <param name="rootCommand">  The root command. </param>
    private static void DefineExecution( RootCommand rootCommand )
    {
        Option<string> basicOption = ( Option<string> ) rootCommand.Options.FirstOrDefault( x => x.Name == "option" );
        if ( basicOption is not null )
        {
            rootCommand.SetHandler( ( option ) =>
            {
                Console.WriteLine( $"Running the '{option}' option..." );
                switch ( option.ToUpperInvariant() )
                {
                    case "SERILOG":
                        RunSerilogImplementation();
                        break;

                    default:
                        break;
                }
                Console.Write( "\r\n\r\nDone. Enter key: " );
                _ = Console.ReadKey();
            },
            basicOption );
        }
        else
        {
            string helpText = rootCommand.Options.FirstOrDefault( x => x.Name == "help" ).Description;
            Console.WriteLine( $"Logger option not specified; {Environment.NewLine}{helpText}" );
        }
    }

    #region " Define Log Messages "

    /// <summary>   Logs a debug message. </summary>
    /// <remarks>   2024-07-25. </remarks>
    /// <param name="logger">   The logger. </param>
    /// <param name="message">  The message. </param>
    [LoggerMessage( 1, LogLevel.Debug, "{message}", EventName = "LogDebugMessage" )]
    internal static partial void LogDebugMessage( Microsoft.Extensions.Logging.ILogger logger, string message );

    /// <summary>   Logs an information message. </summary>
    /// <remarks>   2024-07-25. </remarks>
    /// <param name="logger">   The logger. </param>
    /// <param name="message">  The message. </param>
    [LoggerMessage( 2, LogLevel.Information, "{message}", EventName = "LogInfoMessage" )]
    internal static partial void LogInfoMessage( Microsoft.Extensions.Logging.ILogger logger, string message );

    /// <summary>   Logs an information message. </summary>
    /// <remarks>   2024-07-25. </remarks>
    /// <param name="logger">   The logger. </param>
    /// <param name="message">  The message. </param>
    [LoggerMessage( 3, LogLevel.Warning, "{message}", EventName = "LogWarningMessage" )]
    internal static partial void LogWarningMessage( Microsoft.Extensions.Logging.ILogger logger, string message );

    /// <summary>   Logs an error message. </summary>
    /// <remarks>   2024-07-25. </remarks>
    /// <param name="logger">   The logger. </param>
    /// <param name="message">  The message. </param>
    [LoggerMessage( 4, LogLevel.Error, "{message}", EventName = "LogErrorMessage" )]
    internal static partial void LogErrorMessage( Microsoft.Extensions.Logging.ILogger logger, string message, Exception exception );

    #endregion

    #region " command line parsing "

    /// <summary>   Creates the root command. </summary>
    /// <remarks>   2023-04-18. </remarks>
    /// <returns>   The new root command. </returns>
    private static RootCommand CreateRootCommand()
    {
        // Instantiate the command line application
        RootCommand app = new()
        {
            // This should be the name of the executable itself. the help text line "Usage: ConsoleArgs" uses this
            Name = "LoggingTester",
            Description = ".NET Core logging tester",
            // ExtendedHelpText = @"This program demonstrates the logging functionality of the cc.isr.Logging.Logger."
        };
        return app;
    }

    /// <summary>   Define command line options. </summary>
    private static void DefineCommandLIneOptions( RootCommand rootCommand )
    {
        // Arguments: -o listener -l error -d ..\_log -f Column

        // The first argument is the option template.
        // It starts with a pipe-delimited list of option flags/names to use
        // Optionally, It is then followed by a space and a short description of the value to specify.
        // e.g. here we could also just use "-o|--option"
        rootCommand.AddOption( new Option<string>( ["-o", "--option"], "Listener or Provider Logging implementation" ) );
        rootCommand.AddOption( new Option<LogLevel>( ["-l", "--level"], "Logging level: Information, Warning, Error" ) );
        rootCommand.AddOption( new Option<DirectoryInfo>( ["-d", "--dir"], "Relative Directory, e.g., ..\\_log" ) );
        rootCommand.AddOption( new Option<string>( ["-f", "--form"], "Logging format: Column, Row, Tab, Comma" ) );
    }

    #endregion

    #region " serilog implementation "

    /// <summary>   Gets or sets the number of expected trace messages. </summary>
    /// <value> The number of expected trace messages. </value>
    internal static int ExpectedTraceMessagesCount { get; set; }

    /// <summary>   Adds a trace listener. </summary>
    /// <remarks>   2024-07-24. </remarks>
    /// <param name="addAsyncTraceListener">    True to add asynchronous trace listener. </param>
    private static void AddTraceListener( bool addAsyncTraceListener )
    {
        Console.WriteLine( addAsyncTraceListener ? "Tracing using async queue." : "Tracing using concurrent queue." );
        if ( addAsyncTraceListener )
        {
            cc.isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( cc.isr.Tracing.TracingPlatform.Instance.CriticalTraceEventTraceListener );
            // comment out to see all messages.
            // TracingPlatform.Instance.CriticalTraceEventSourceLevel = TracingPlatform.Instance.TraceEventSourceLevel;
        }
        else
        {
            // Add Concurrent Queue trace listener
            cc.isr.Tracing.TracingPlatform.Instance.AddTraceListener( cc.isr.Tracing.TracingPlatform.Instance.CriticalTraceEventTraceListener );
        }
    }

    /// <summary>   Executes the 'provider implementation' operation. </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    private static void RunSerilogImplementation()
    {
        // set the runtime serilog level.
        Orlogger.Instance.RuntimeSerilogLevel = Microsoft.Extensions.Logging.LogLevel.Trace;

        Console.WriteLine( $"Trace level set at '{Orlogger.Instance.RuntimeSerilogLevel}'." );

        // build the Serilog settings file name.

        string fileName = Orlogger.BuildEntryAssemblySettingsFileName();

        // Create a service collection and add the Serilog services to this collection of services.

        IServiceCollection serviceCollection = Orlogger.ConfigureLoggingServices( fileName, Orlogger.Instance.SerilogLevelSwitch, Orlogger.Instance.TraceSourceSwitch,
                                                                                  LoggingProviderKinds.Serilog | LoggingProviderKinds.ConsoleLog
                                                                                  | LoggingProviderKinds.DebugLog | LoggingProviderKinds.TraceLog );

        // add async trace listener

        Program.AddTraceListener( true );

        // Add a transient service of the type specified in ITestService with the implementation type specified in TestService.
        _ = serviceCollection.AddTransient<ITestService, TestService>();

        // Add a transient service of the type specified in the App class to the collection of service.
        _ = serviceCollection.AddTransient<App>();

        // Create service provider
        Orlogger.Instance.LoggingServicesProvider = serviceCollection.BuildServiceProvider();

        // Create service provider
        // ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

        // a logger can be instantiated at this point. 
        // Microsoft.Extensions.Logging.ILogger<Program> logger = Orlogger.Instance.LoggingServicesProvider.GetService<ILoggerFactory>().CreateLogger<Program>();

        // a logger can be instantiated at this point. 
        Microsoft.Extensions.Logging.ILogger<Program> logger = Orlogger.Instance.CreateLogger<Program>()!;

        // count expected trace messages based on the listener level.
        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
            Program.ExpectedTraceMessagesCount += 1;

        SelfLog.Enable( Console.Error );

        TargetFrameworkAttribute framework = Assembly.GetEntryAssembly().GetCustomAttribute<TargetFrameworkAttribute>();
        System.Reflection.Assembly executingAssembly = System.Reflection.Assembly.GetExecutingAssembly();
        Program.LogInfoMessage( logger, $"Starting {executingAssembly.FullName}." );
        Program.LogInfoMessage( logger, $"  running under {framework?.FrameworkName}." );

        // entry to inject the logger and run the application
        // this rebuilds the service provider thus adding any service targets that were 
        // added after the previous service provider was built.
        serviceCollection.BuildServiceProvider().GetService<App>()!.Run();

        // entry to run the application
        // serviceProvider.GetService<App>().Run();
        // Orlogger.Instance.LoggingServicesProvider.GetService<App>().Run();
    }

    #endregion
}

#region " logger-injected application classes "

/// <summary>
/// To maintain a separation of concern between our business based logic and the logic we use to
/// configure and run the actual console application, let's create a new class called App.cs. The
/// goal being that we will use Program.cs to bootstrap everything need to support our
/// application and then fire off all the logic that is needed to support out "business" needs by
/// way of executing the Run() method in App.cs.
/// </summary>
/// <remarks>
/// <see href="https://learn.microsoft.com/en-us/dotnet/core/extensions/high-performance-logging"/>
/// <see href="https://stackoverflow.com/questions/75893617/loggermessage-structured-logging"/>
/// <see href="https://github.com/dotnet/aspnetcore/blob/2575e22efba6d07862b951fbe50ff28659e11ee7/src/SignalR/clients/csharp/Client.Core/src/HubConnection.Log.cs#L15"/>
/// </remarks>
/// <remarks>   David, 2021-02-04. </remarks>
public class App( ITestService testService, Microsoft.Extensions.Logging.ILogger<App> logger )
{
    private readonly ITestService _testService = testService;
    private readonly Microsoft.Extensions.Logging.ILogger<App> _logger = logger;

    /// <summary>   Runs this object. </summary>
    /// <remarks>   2024-07-25. </remarks>
    public void Run()
    {
        Program.LogInfoMessage( this._logger, "Running application." );

        // count expected trace messages based on the listener level.

        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
            Program.ExpectedTraceMessagesCount += 1;

        TimeSpan timeout = TimeSpan.FromMilliseconds( 100 );
        bool fileOpened = HeaderWriter.AwaitFullFileName( timeout, TimeSpan.FromMilliseconds( 20 ), TimeSpan.FromMilliseconds( 20 ) );
        string message = fileOpened
            ? $"Log {HeaderWriter.FullFileName}.".Replace( "\\", "/" )
            : $"File not open after {timeout.TotalMilliseconds}ms.";
        Program.LogInfoMessage( this._logger, message );

        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
            Program.ExpectedTraceMessagesCount += 1;

        try
        {
            this._testService.Run();
        }
        catch ( Exception ex )
        {
            Program.LogWarningMessage( this._logger, "warning; exception" );
            if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
                Program.ExpectedTraceMessagesCount += 1;

            // add exception data.
            // 
            ex.Data.Add( $"{ex.Data.Count}-internal1", "internal exception data" );
            ex.Data.Add( $"{ex.Data.Count}-internal2", "internal exception data 2" );
            InvalidOperationException reportEx = new( $"Caught in {nameof( App )}", ex );
            reportEx.Data.Add( $"{reportEx.Data.Count}-parent1", "parent exception data" );
            reportEx.Data.Add( $"{reportEx.Data.Count}-parent2", "parent exception data 2" );

            Program.LogErrorMessage( this._logger, "reporting exception", reportEx );
            if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
                Program.ExpectedTraceMessagesCount += 1;
        }

        Console.WriteLine();
        Console.WriteLine( "--- Issue 184: Debug and trace events are filtered in even under warnings level." );
        Console.WriteLine( $"--- Queue Trace Listener Records (expecting {Program.ExpectedTraceMessagesCount} or {Program.ExpectedTraceMessagesCount + 1}):" );
        // Console write line missed the CRLF preceding the stack trace line.
        string formatted = TracingPlatform.Instance.CriticalTraceEventTraceListener.DequeueAllMessages();
        Console.WriteLine( formatted );
        Console.WriteLine( "--- End of Queue Trace Listener Records" );
        Console.WriteLine();

        // report number of elements in the Queue Trace Listener
        Console.WriteLine( $"Queue Trace Listener includes {TracingPlatform.Instance.CriticalTraceEventTraceListener.Queue.Count} records (expecting {Program.ExpectedTraceMessagesCount} or {Program.ExpectedTraceMessagesCount + 1})." );
        Console.WriteLine( $"Flushing the Queue Trace Listener ." );
        TracingPlatform.Instance.CriticalTraceEventTraceListener.Flush();
        Console.WriteLine( $"Queue Trace Listener includes {TracingPlatform.Instance.CriticalTraceEventTraceListener.Queue.Count} records after flush." );

        Console.WriteLine();
        Console.WriteLine( "--- Queue Trace Listener Records after flush:" );
        // Console write line missed the CRLF preceding the stack trace line.
        formatted = TracingPlatform.Instance.CriticalTraceEventTraceListener.DequeueAllMessages();
        Console.WriteLine( formatted );
        Console.WriteLine( "--- End of Queue Trace Listener Records" );
        Console.WriteLine();

        Log.CloseAndFlush();
    }
}
/// <summary>   Interface for test service. </summary>
/// <remarks>   David, 2021-02-04. </remarks>
public interface ITestService
{
    void Run();
}
/// <summary>   A service for accessing tests information. </summary>
/// <remarks>   David, 2021-02-04. </remarks>
internal sealed partial class TestService( Microsoft.Extensions.Logging.ILogger<TestService> logger ) : ITestService
{
    private readonly Microsoft.Extensions.Logging.ILogger<TestService> _logger = logger;

    /// <summary>   Runs this object. </summary>
    /// <remarks>   2024-07-25. </remarks>
    /// <exception cref="DivideByZeroException">    Thrown when an attempt is made to divide a number
    ///                                             by zero. </exception>
    public void Run()
    {
        Program.LogDebugMessage( this._logger, "Running logging service." );
        // this._logger.LogDebug( $"Running logging service." );
        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Verbose )
            Program.ExpectedTraceMessagesCount += 1;

        Program.LogDebugMessage( this._logger, "Throwing divide by zero exception." );
        // this._logger.LogDebug( $"Throwing divide by zero exception." );
        if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Verbose )
            Program.ExpectedTraceMessagesCount += 1;

        // demo reporting an exception.
        throw new DivideByZeroException();
    }
}

#endregion
