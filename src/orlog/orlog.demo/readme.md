# Serilog Test Program

This test program demonstrates how to how to enable logging in a .NET Core Console application.

## Serilog
This demonstration uses the SERILOG framework for logging in .NET 5.0.

ILogger interface works very nicely with the .NET Core ecosystem and today
in this post we will learn on 

## Dependency Injection
In .NET core, projects logging is managed via dependency injection. 
Whilst this works fine for ASP.NET projects, where this is all automatically
created upon starting a new project in Startup.cs, in console applications 
it takes a bit of configuration to get it up and running.

## Microsoft Extensions Logging
.NET Core has introduced ILogger as a generic interface for logging purposes. 
This framework supported interface ILogger can be used across different types 
of applications like, Console, Asp.Net, Desktop or Form Applications.

Unlike ASP.NET Core, Console applications do not have dependency injection by 
default. Explicit dependency injection can be implemented upon starting a 
Console application.

## Demonstration Design

Ann Application (App) class is used to maintain a separation of concern between 
the business-based logic and the logic used to configure and run the actual console 
application. This startup Program class bootstraps everything needed to support 
the application, which then fire off all the logic that is needed to support 
the "business" needs by way of executing the Run() method in the App class. 

In this case, the business logic is implemented by the Test Service class.

The dependency injection container provides the means for registering the
individual components used in the program. To this end, a new service called 
TestService is registered. 

The App class receives an object that meets the ITestService interface contract. 
This object will be passed in through the dependency manager. 

The ServiceCollection object is configured adding the dependencies to the container 
collection, of which can have a lifetime of Scoped, Transient or Singleton. 
Once the ServiceCollection object is configured, an IServiceProvider 
(Dependency Management Container) is requested from the ServiceCollection object 
in order to manually resolve the App class and kick a logical loop off by calling 
its Run() method. 

## Logging

Logging is set up once the Dependency Injection is wired up. More specifically, 
a log to the console is added to visually verify that not only App.Run() method 
is being called but also that the ITestService is being injected and ran from 
the App class. 

This is accomplished by adding new instances of ILoggerFactory for the Console 
(outputs to the console) and Debug (writes log output by way of System.Diagnostics.Debug) 
providers as well as the Serilog provider. These providers have a Singleton lifetime. 
Finally, the logging services are added to the service collection. 

## Running
The instance of the ILogger<App>, which was injected to App is used to 
LogInfromation inside the App Run() method. Additionally, the ITestService.Run() 
of the injected service is called to to further illustrate that the dependency 
injection is actually managing dependencies correctly. 

Much like App, an instance of ILogger<TestService>, which was injected into the 
Test Service class, performs a LogWarning inside of ITestService.Run().

## Configuration
Configuration is accomplished through a JSON file. 

A ConfigurationBuilder object is used to:
* Set an absolute path scoped to the directory where appsettings.json file lives.  
* The appsettings.json file is added to the IConfigurationRoot object, denoting it as not optional.
* The configuration is built returning an IConfigurationRoot object.

Thereafter, the configuration is applied to the Serilog logger. 

With Console and Debug loggers, a call to AddOptions() on our IServiceCollection 
object adds services needed to use the Options pattern inside of the App. In short, 
the Options Pattern allows decoupling feature configuration in the application and 
bind said feature configuration to independent models.

## Serilog
Serilog is a newer logging framework for .NET. It was built with structured logging 
in mind. It makes it easy to record custom object properties and even output logs to 
numerous destinations called 'Sinks'.

# Sinks
Sinks are how you direct where you want your logs sent. The most popular of the 
standard sinks are the File and Console targets. I would also try the Debug sink 
if you want to see your log statements in the Visual Studio Debug window so you 
don’t have to open a log file.  

Serilog’s sinks are configured in code when your application first starts.
Here is an example:

```
using (var log = new LoggerConfiguration()  
    .WriteTo.Console()  
    .CreateLogger())  
{
    log.Information("Hello, Serilog!");  
    log.Warning("Goodbye, Serilog.")  
}
```

# Serilog’s internal debug logging

If you are having any problems with Serilog, you can subscribe to it’s internal 
events and write them to your debug window or a console.

```
Serilog.Debugging.SelfLog.Enable(message => Debug.WriteLine(message));
Serilog.Debugging.SelfLog.Enable(Console.Error);
```
Please note that the internal logging will not write to any user-defined sinks.

# Log levels and filtering
Be sure to use verbose, debug, information, warning, error, and fatal logging 
levels as appropriate. This is really valuable if you want to specify only certain
levels to be logged to specific logging sinks or to reduce logging in production.

If you are using a central logging solution, you can easily search for logs by the 
logging level. This makes it easy to find warnings, errors, and fatals quickly.

# Structured logging

Serilog was designed to easily log variables in your code:

```
Log.Debug("Processing item {ItemNumber} of {ItemCount}", itemNumber, itemCount);
```
Serilog takes it to the next level because those same variables can also easily 
be recorded as JSON or sent to a log management solution like [Retrace].

If you want to really get the value of [structured logging], you will want to send 
your logs to a [log management] tool that can index all the fields and enable powerful 
searching and analytics capabilities.

# Sending alerts for exceptions

If you want to send alerts when a new exception occurs, send your exceptions to an 
[error reporting] solution, like [Retrace], that is designed for this. 
[Retrace] can filter duplicate your errors so you can figure out when an error is 
truly new or regressed. You can also track its history, error rates, and a bunch of 
other cool things.

# Use filters to suppress certain logging statements

Serilog has the ability to specify log levels to send to specific sinks or 
suppress all log messages. Use the *Restricted To Minimum Level* parameter.
```
Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .WriteTo.File("log.txt")
    .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
    .CreateLogger();
```

# Make your own custom sinks

If you want to do something that the standard Serilog sinks do not support, you can 
search online for one or write your own. One example could be a target for writing 
to Azure Storage.  

As an example of a custom target, you can review the [source code for our Serilog sink]
for sending logs to [Retrace].

# Customize the output format of your Logs

With Serilog you can control the format of your logs, such as which fields you 
include, their order, and etc.

Here is a simple example:
```
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console(outputTemplate:
        "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
    .CreateLogger();
```
The following fields can be used in a custom output template:
```
    Exception
    Level
    Message
    NewLine
    Properties
    Timestamp
```
If that isn’t enough, you can implement ITextFormatter to have even more control of
the output format.  

Serilog has also great support from writing your log files as JSON. It has a built-in 
JSON formatter that you can use.
```
Log.Logger = new LoggerConfiguration()
    .WriteTo.File(new CompactJsonFormatter(), "log.txt")
    .CreateLogger();
```

## File name date and logging timestamp
Because, presently, [Serilog.Sinks.File] uses local time for setting the rolling file name by date,
the timestamp used inside this file should also be set to local time. For completeness,
the timestamp also includes the local time offset from universal time. 


# Enrich your logs with more context

Most logging frameworks provide some way to log additional context values across
all logging statements. This is perfect for setting something like a UserId at the
beginning for a request and having it included in every log statement.  

Serilog implements this by what they call enrichment.

Below is a simple example of how to add the current thread id to the logging data captured.
```
var log = new LoggerConfiguration()
    .Enrich.WithThreadId()
    .WriteTo.Console()
    .CreateLogger();
```
To really use enrichment with your own app, you will want to use the LogContext.
```
var log = new LoggerConfiguration()
    .Enrich.FromLogContext()
```
After configuring the log context, you can then use it to push properties into the
context of the request. Here is an example:
```
log.Information("No contextual properties");
using (LogContext.PushProperty("A", 1))
{
    log.Information("Carries property A = 1");
    using (LogContext.PushProperty("A", 2))
    using (LogContext.PushProperty("B", 1))
    {
        log.Information("Carries A = 2 and B = 1");
    }
    log.Information("Carries property A = 1, again");
}
```

# Json Configuration
See [Tech Repository] and [serilog settings configuration] and [C-Sharp Corner]

A file name can be defined as Rolling:
```
    "WriteTo": [
      { "Name": "Console" },
      {
        "Name": "File",
        "Args": {
          "path": "../_Logs/ex_.log",
          "outputTemplate": "{Timestamp:o} [{Level:u3}] ({SourceContext}) {Message}{NewLine}{Exception}",
          "rollingInterval": "Day",
          "retainedFileCountLimit": 7
        }
      }
    ],
```

In XML and JSON configuration formats, environment variables can be used in setting values. This means, for instance, that the log file path 
can be based on TMP or APPDATA:
```
value="%APPDATA%\MyApp\log.txt" />
"path": "%APPDATA%/MyApp/_Logs/ex_.log",
```

# Getting the active file name
The file name is updated upon writing the header and can be had from the Header Writer class. 


## References
[bitscry]  
[Chad Ramos]
[Stackify]

[bitscry]: https://blog.bitscry.com/2017/05/31/logging-in-net-core-console-applications//
[Chad Ramos]: https://pioneercode.com/post/dependency-injection-logging-and-configuration-in-a-dot-net-core-console-app
[Stackify]: https://stackify.com/serilog-tutorial-net-logging/
[Retrace]: https://stackify.com/retrace/
[Structured Logging]: https://stackify.com/what-is-structured-logging-and-why-developers-need-it/
[log management]: https://stackify.com/retrace-log-management/
[error reporting]: https://stackify.com/error-reporting/
[source code for our Serilog sink]: https://github.com/stackify/Serilog.Sinks.Stackify/blob/master/Serilog.Sinks.Stackify/Sinks/Stackify/StackifySink.cs
[Tech Repository]: https://www.techrepository.in/blog/posts/writing-logs-to-different-files-serilog-asp-net-core
[serilog settings configuration]: https://github.com/serilog/serilog-settings-configuration
[C-Sharp Corner]: https://www.c-sharpcorner.com/article/serilog-in-dotnet-core/
[Issue #154]: https://github.com/serilog/serilog-sinks-file/issues/154
[Serilog.Sinks.File]: https://github.com/serilog/serilog-sinks-file
[Serilog.Sinks.File.Header]: https://github.com/cocowalla/serilog-sinks-file-header
