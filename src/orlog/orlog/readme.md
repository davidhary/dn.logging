# About

[cc.isr.Logging.Orlog]  is a .Net library for Serliog Logging.

# How to Use

# Key Features

* Provides classes for logging.

# Main Types

The main types provided by this library are:

* _tba_ 

# Feedback

[cc.isr.Logging.Orlog] is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Logging Repository].

[Logging Repository]: https://bitbucket.org/davidhary/dn.logging
[cc.isr.Logging.Orlog]: https://bitbucket.org/davidhary/dn.logging/src/orlog/cc.isr.Logging.Orlog

