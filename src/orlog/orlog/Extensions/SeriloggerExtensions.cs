namespace cc.isr.Logging.Extensions;

/// <summary>   <see cref="Serilog"/>.<see cref="Serilog.ILogger"/> extension methods. </summary>
/// <remarks>   David, 2021-06-28. </remarks>
public static class SeriloggerExtensions
{
    /// <summary>   A Serilog.ILogger extension method that adds logging context. </summary>
    /// <remarks>
    /// David, 2021-07-02.
    /// <see href="https://StackOverflow.com/questions/29470863/SERILOG-output-enrich-all-messages-with-MethodName-from-which-log-entry-was-ca"/>.
    /// </remarks>
    /// <param name="logger">           The logger to act on. </param>
    /// <param name="memberName">       (Optional) Name of the member. </param>
    /// <param name="sourceFilePath">   (Optional) Full pathname of the source file. </param>
    /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
    /// <returns>   A <see cref="Serilog.ILogger"/>. </returns>
    public static Serilog.ILogger Here( this Serilog.ILogger logger,
        [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0 )
    {
        return logger
            .ForContext( "MemberName", memberName )
            .ForContext( "FilePath", sourceFilePath )
            .ForContext( "LineNumber", sourceLineNumber );
    }

}
