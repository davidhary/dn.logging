using System.Diagnostics;

namespace cc.isr.Logging.Extensions;

/// <summary>   A trace event type extensions. </summary>
/// <remarks>   2024-07-26. </remarks>
public static class TraceEventTypeExtensions
{
    /// <summary>   Converts a <see cref="System.Diagnostics.TraceEventType"/> level to
    /// <see cref="Microsoft.Extensions.Logging.LogLevel"/>. </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    /// <param name="level">    The level. </param>
    /// <returns>   Level as a <see cref="Microsoft.Extensions.Logging.LogLevel"/>. </returns>
    public static Microsoft.Extensions.Logging.LogLevel ToLogLevel( this System.Diagnostics.TraceEventType level )
    {
        return 0 != (level & TraceEventType.Critical)
            ? Microsoft.Extensions.Logging.LogLevel.Critical
            : 0 != (level & TraceEventType.Error)
            ? Microsoft.Extensions.Logging.LogLevel.Error
            : 0 != (level & TraceEventType.Warning)
            ? Microsoft.Extensions.Logging.LogLevel.Warning
            : 0 != (level & TraceEventType.Information)
            ? Microsoft.Extensions.Logging.LogLevel.Information
            : 0 != (level & TraceEventType.Verbose)
            ? Microsoft.Extensions.Logging.LogLevel.Trace
            : Microsoft.Extensions.Logging.LogLevel.Information;
    }

}
