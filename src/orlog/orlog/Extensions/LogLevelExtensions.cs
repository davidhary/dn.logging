namespace cc.isr.Logging.Extensions;

/// <summary>   A log level extensions. </summary>
/// <remarks>   2024-07-26. </remarks>
public static class LogLevelExtensions
{
    /// <summary>
    /// Converts a <see cref="Microsoft.Extensions.Logging.LogLevel"/> level to
    /// <see cref="System.Diagnostics.TraceEventType"/>.
    /// </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    /// <param name="level">    The level. </param>
    /// <returns>   Level as the <see cref="System.Diagnostics.TraceEventType"/>. </returns>
    public static System.Diagnostics.TraceEventType ToTraceEventType( this Microsoft.Extensions.Logging.LogLevel level )
    {
        return level switch
        {
            Microsoft.Extensions.Logging.LogLevel.Critical => System.Diagnostics.TraceEventType.Critical,
            Microsoft.Extensions.Logging.LogLevel.Debug => System.Diagnostics.TraceEventType.Transfer,
            Microsoft.Extensions.Logging.LogLevel.Error => System.Diagnostics.TraceEventType.Error,
            Microsoft.Extensions.Logging.LogLevel.Information => System.Diagnostics.TraceEventType.Information,
            Microsoft.Extensions.Logging.LogLevel.Trace => System.Diagnostics.TraceEventType.Verbose,
            Microsoft.Extensions.Logging.LogLevel.Warning => System.Diagnostics.TraceEventType.Warning,
            Microsoft.Extensions.Logging.LogLevel.None => System.Diagnostics.TraceEventType.Information,
            _ => System.Diagnostics.TraceEventType.Information,
        };
    }

}
