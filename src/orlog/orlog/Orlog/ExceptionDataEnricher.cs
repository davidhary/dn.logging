using System.Collections;
using Serilog.Core;

namespace cc.isr.Logging.Orlog;

/// <summary>   An exception data enricher. </summary>
/// <remarks>   David, 2021-02-17.
/// <see href="https://gist.GitHub.com/NBLUMHARDT/DDDAA2139BBF4B561FA7"/></remarks>
public class ExceptionDataEnricher : ILogEventEnricher
{
    /// <summary>   Enrich the log event. </summary>
    /// <remarks>   David, 2021-02-17. </remarks>
    /// <param name="logEvent">         The log event to enrich. </param>
    /// <param name="propertyFactory">  Factory for creating new properties to add to the event. </param>
    public void Enrich( Serilog.Events.LogEvent logEvent, ILogEventPropertyFactory propertyFactory )
    {
        if ( logEvent.Exception == null ||
            logEvent.Exception.Data == null ||
            logEvent.Exception.Data.Count == 0 ) return;

        Dictionary<string, object> dataDictionary = logEvent.Exception.Data
            .Cast<DictionaryEntry>()
            .Where( e => e.Key is string )
            .ToDictionary( e => ( string ) e.Key, e => e.Value );

        Serilog.Events.LogEventProperty property = propertyFactory.CreateProperty( "ExceptionData", dataDictionary, destructureObjects: true );

        logEvent.AddPropertyIfAbsent( property );
    }

}
