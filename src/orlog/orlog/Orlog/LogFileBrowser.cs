using System.Diagnostics;

namespace cc.isr.Logging.Orlog;

/// <summary>   A log file browser. </summary>
/// <remarks>   2024-07-29. </remarks>
public static class TraceLogBrowser
{
    /// <summary>   Gets the filename of the logger. </summary>
    /// <remarks> This gets updated after the first info goes into the file.</remarks>
    /// <value> The filename logger. </value>
    public static string FullLogFileName => HeaderWriter.FullFileName;

    /// <summary> Opens log file. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <returns> The Process. </returns>
    public static (bool Success, string Details, Process? process) OpenLogFile()
    {
        if ( string.IsNullOrEmpty( HeaderWriter.FullFileName ) )
            return (false, "log file name not created", null);

        FileInfo fi = new( HeaderWriter.FullFileName );
        if ( !fi.Exists )
            return (false, $"log file {fi.FullName} not found", null);

        string proc = "explorer.exe";
        string args = $"\"{HeaderWriter.FullFileName}\"";
        return (true, string.Empty, Process.Start( proc, args ));
    }

    /// <summary> Opens folder location. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <returns> The Process. </returns>
    public static (bool Success, string Details, Process? process) OpenFolderLocation()
    {
        if ( string.IsNullOrEmpty( HeaderWriter.FullFileName ) )
            return (false, "log file name not created", null);

        FileInfo fi = new( HeaderWriter.FullFileName );
        if ( !fi.Directory.Exists )
            return (false, $"log directory {fi.FullName} not found", null);

        string proc = "explorer.exe";
        string args = $"\"{fi.DirectoryName}\"";
        return (true, string.Empty, Process.Start( proc, args ));
    }
}
