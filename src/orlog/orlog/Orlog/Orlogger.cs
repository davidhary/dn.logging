using System.Diagnostics;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace cc.isr.Logging.Orlog;

/// <summary>   An Orlogger. </summary>
/// <remarks>   2024-07-24. </remarks>
public class Orlogger
{
    #region " construction and cleanup "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    private Orlogger()
    {
        this.SerilogLevelSwitch = new Serilog.Core.LoggingLevelSwitch( Serilog.Events.LogEventLevel.Debug );
        this.TraceSourceSwitch = new System.Diagnostics.SourceSwitch( TRACE_SOURCE_SWITCH_NAME )
        {
            Level = System.Diagnostics.SourceLevels.Information
        };
    }

    #region " disposable support "

    /// <summary> Gets the disposed indicator. </summary>
    /// <value> The disposed indicator. </value>
    public bool IsDisposed { get; private set; }

    /// <summary>
    /// Releases the unmanaged resources used by the class.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="disposing"> True to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected virtual void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
                this.LoggingServicesProvider?.Dispose();
        }
        finally
        {
            this.IsDisposed = true;
        }
    }

    /// <summary> Calls <see cref="Dispose(bool)" /> to cleanup. </summary>
    /// <remarks>
    /// Do not make this method Overridable (virtual) because a derived class should not be able to
    /// override this method.
    /// </remarks>
    public void Dispose()
    {
        this.Dispose( true );
        // Take this object off the finalization(Queue) and prevent finalization code 
        // from executing a second time.
        GC.SuppressFinalize( this );
    }

    /// <summary>
    /// This destructor will run only if the Dispose method does not get called. It gives the base
    /// class the opportunity to finalize. Do not provide destructors in types derived from this
    /// class.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    ~Orlogger()
    {
        // Do not re-create Dispose clean-up code here.
        // Calling Dispose(false) is optimal for readability and maintainability.
        this.Dispose( false );
    }

    #endregion

    #endregion

    #region " lazy singleton "

    /// <summary> The singleton instance </summary>
    private static readonly Orlogger? _instance = new();

    /// <summary> Instantiates the default instance of the class. </summary>
    /// <remarks> Use this property to instantiate a single instance of this class. This class uses
    /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved or if is was disposed. </remarks>
    /// <returns> A new or existing instance of the class. </returns>
    public static Orlogger Instance => _instance!;

    #endregion

    #region " trace switch "

    /// <summary>   Name of the trace source switch. </summary>
    public const string TRACE_SOURCE_SWITCH_NAME = "TraceSourceSwitch";

    /// <summary>
    /// Gets the Trace source switch, which controls the trace source if using Microsoft rather than
    /// Serilog tracing.
    /// </summary>
    /// <value> The trace source switch. </value>
    public System.Diagnostics.SourceSwitch TraceSourceSwitch { get; }

    /// <summary>   Gets or sets the source level above which messages are ignored. </summary>
    /// <value> The source level above which messages are ignored. </value>
    public System.Diagnostics.SourceLevels RuntimeTraceLevel
    {
        get => this.TraceSourceSwitch.Level;
        set
        {
            if ( value != this.RuntimeTraceLevel )
                this.TraceSourceSwitch.Level = value;
        }
    }

    #endregion

    #region " serilog log level switch "

    /// <summary>   Gets the Serilog level switch. </summary>
    /// <value> The Serilog level switch. </value>
    public Serilog.Core.LoggingLevelSwitch SerilogLevelSwitch { get; }

    /// <summary>   Converts a <see cref="LogLevel"/> level to
    /// <see cref="Serilog.Events.LogEventLevel"/>. </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    /// <param name="level">    The level. </param>
    /// <returns>   Level as a Serilog.Events.LogEventLevel. </returns>
    public static Serilog.Events.LogEventLevel ToSerilogEventLevel( LogLevel level )
    {
        return level switch
        {
            LogLevel.Critical => Serilog.Events.LogEventLevel.Fatal,
            LogLevel.Debug => Serilog.Events.LogEventLevel.Debug,
            LogLevel.Error => Serilog.Events.LogEventLevel.Error,
            LogLevel.Information => Serilog.Events.LogEventLevel.Information,
            LogLevel.Trace => Serilog.Events.LogEventLevel.Verbose,
            LogLevel.Warning => Serilog.Events.LogEventLevel.Warning,
            LogLevel.None => Serilog.Events.LogEventLevel.Information,
            _ => Serilog.Events.LogEventLevel.Information,
        };
    }

    /// <summary>   Converts a level to a log level. </summary>
    /// <remarks>   David, 2021-08-13. </remarks>
    /// <param name="level">    The level. </param>
    /// <returns>   Level as a Serilog.Events.LogEventLevel. </returns>
    public static LogLevel ToLogLevel( Serilog.Events.LogEventLevel level )
    {
        return level switch
        {
            Serilog.Events.LogEventLevel.Verbose => LogLevel.Trace,
            Serilog.Events.LogEventLevel.Debug => LogLevel.Debug,
            Serilog.Events.LogEventLevel.Information => LogLevel.Information,
            Serilog.Events.LogEventLevel.Warning => LogLevel.Warning,
            Serilog.Events.LogEventLevel.Error => LogLevel.Error,
            Serilog.Events.LogEventLevel.Fatal => LogLevel.Critical,
            _ => LogLevel.Information,
        };
    }

    /// <summary>
    /// Gets or sets the runtime Serilog minimum level Threshold.
    /// </summary>
    /// <value> The log level. </value>
    public LogLevel RuntimeSerilogLevel
    {
        get => ToLogLevel( this.SerilogLevelSwitch.MinimumLevel );
        set
        {
            if ( value != this.RuntimeSerilogLevel )
                this.SerilogLevelSwitch.MinimumLevel = ToSerilogEventLevel( value );
        }
    }

    #endregion

    #region " settings file name "

    /// <summary>
    /// Builds application settings file title and extension consisting of the
    /// <paramref name="assemblyName"/>+<paramref name="suffix"/>+.json.
    /// </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="assemblyName"> The assembly name. </param>
    /// <param name="suffix">       (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A file title and extension. </returns>
    public static string BuildAppSettingsFileName( string assemblyName, string suffix = ".Serilog" )
    {
        return $"{assemblyName}{suffix}.json";
    }

    /// <summary>
    /// Builds application settings file name consisting of the <para>
    /// 'Assembly Name'+<paramref name="suffix"/>+.json.</para>
    /// </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="assembly"> The assembly, which could be the calling, executing or entry assembly. </param>
    /// <param name="suffix">   (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildAppSettingsFileName( System.Reflection.Assembly assembly, string suffix = ".Serilog" )
    {
        return BuildAppSettingsFileName( assembly.GetName().Name, suffix );
    }

    /// <summary>   Builds calling assembly settings file name. </summary>
    /// <remarks>   David, 2021-07-01. </remarks>
    /// <param name="suffix">   (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildCallingAssemblySettingsFileName( string suffix = ".Serilog" )
    {
        return BuildAppSettingsFileName( System.Reflection.Assembly.GetCallingAssembly().GetName().Name, suffix );
    }

    /// <summary>   Builds entry assembly log settings file name. </summary>
    /// <remarks>   David, 2021-07-01. </remarks>
    /// <param name="suffix">   (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A file title and extension. </returns>
    public static string BuildEntryAssemblySettingsFileName( string suffix = ".Serilog" )
    {
        // the entry assembly might be null when called from the test platform, 
        // because the entry assembly must be a managed type, which the test platform might not.
        System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly() is null
                                                ? System.Reflection.Assembly.GetEntryAssembly() is null
                                                    ? System.Reflection.Assembly.GetCallingAssembly()
                                                    : System.Reflection.Assembly.GetEntryAssembly()
                                                : Orlogger.GetCallingAssemblyByStackTrace( Assembly.GetExecutingAssembly() );
        return BuildAppSettingsFileName( assembly.GetName().Name, suffix );
    }

    /// <summary>
    /// Builds the settings file name of the loaded assembly in which the <paramref name="type"/> is
    /// defined.
    /// </summary>
    /// <remarks>   David, 2021-07-01. </remarks>
    /// <param name="type">     The type which assembly is selected. </param>
    /// <param name="suffix">   (Optional) The suffix, which defaults to '.logSettings'. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildAssemblySettingsFileName( Type type, string suffix = ".Serilog" )
    {
        return BuildAppSettingsFileName( System.Reflection.Assembly.GetAssembly( type ).GetName().Name, suffix );
    }

    /// <summary>   Gets calling assembly by stack trace starting with the executing assembly. </summary>
    /// <remarks>   2024-07-29. <para>
    /// <see href="https://www.codeproject.com/tips/791878/get-calling-assembly-from-stacktrace"/>
    /// </para></remarks>
    /// <returns>   The calling assembly by stack trace. </returns>
    public static Assembly GetCallingAssemblyByStackTrace( Assembly executingAssembly )
    {
        StackTrace stackTrace = new();
        StackFrame[] frames = stackTrace.GetFrames();

        foreach ( StackFrame stackFrame in frames )
        {
            Assembly ownerAssembly = stackFrame.GetMethod().DeclaringType.Assembly;
            if ( ownerAssembly != executingAssembly )
                return ownerAssembly;
        }
        return executingAssembly;
    }

    #endregion

    #region " configure the logger services "

    /// <summary>   Creates a logger. </summary>
    /// <remarks>   2024-07-23. </remarks>
    /// <param name="configuration">    The configuration. </param>
    public static Serilog.ILogger CreateLogger( IConfigurationRoot configuration )
    {
        // Initialize the Serilog logger using Json configuration file.
        return new LoggerConfiguration()
             .ReadFrom.Configuration( configuration )
             .CreateLogger();
    }

    /// <summary>
    /// Create a service collection and configure and add Serilog services to this collection.
    /// </summary>
    /// <remarks>   2024-07-24. </remarks>
    /// <exception cref="FileNotFoundException">    Thrown when the requested file is not present. </exception>
    /// <param name="serviceCollection">    Specifies the contract for a Collection of service descriptors. </param>
    /// <param name="settingsFileName">     Filename of the settings file. </param>
    /// <param name="serilogLevelSwitch">   The Serilog level switch. </param>
    /// <param name="traceSourceSwitch">    The trace source switch. </param>
    /// <param name="providerKinds">        (Optional) The provider kinds; defaults to <see cref="LoggingProviderKinds.Serilog"/>.</param>
    /// <returns>   An IServiceCollection. </returns>
    public static IServiceCollection ConfigureLoggingServices( IServiceCollection serviceCollection,
        string settingsFileName,
        Serilog.Core.LoggingLevelSwitch serilogLevelSwitch,
        System.Diagnostics.SourceSwitch traceSourceSwitch,
        LoggingProviderKinds providerKinds = LoggingProviderKinds.Serilog )
    {
        // check file existence

        FileInfo fi = new( Path.Combine( AppContext.BaseDirectory, settingsFileName ) );
        if ( !fi.Exists )
            throw new FileNotFoundException( $"Logging configuration file '{fi.FullName}' not found using '{AppContext.BaseDirectory}' folder and '{settingsFileName}' file name.", fi.FullName );

        // Build the configuration

        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath( AppContext.BaseDirectory )
            .AddJsonFile( settingsFileName, false )
            .Build();

        // Add logging

        _ = serviceCollection.AddLogging( loggingBuilder =>
        {
            if ( LoggingProviderKinds.Serilog == (LoggingProviderKinds.Serilog & providerKinds) )
                _ = loggingBuilder.AddSerilog();
            if ( LoggingProviderKinds.ConsoleLog == (LoggingProviderKinds.ConsoleLog & providerKinds) )
                _ = loggingBuilder.AddConsole();
            if ( LoggingProviderKinds.DebugLog == (LoggingProviderKinds.DebugLog & providerKinds) )
                _ = loggingBuilder.AddDebug();
            if ( LoggingProviderKinds.TraceLog == (LoggingProviderKinds.TraceLog & providerKinds) )
                _ = loggingBuilder.AddTraceSource( traceSourceSwitch );
            _ = loggingBuilder.AddConfiguration( configuration );
        } );

        // Initialize the Serilog logger using Json configuration file.

        Serilog.Log.Logger = new LoggerConfiguration()
             .ReadFrom.Configuration( configuration )
             // .Enrich.WithExceptionDetails()
             // .Enrich.With<ExceptionDataEnricher>()
             // .WriteTo.File(new JsonFormatter( renderMessage: true ), @"..\_log\log-{Date}.txt" )
             .Enrich.WithDemystifiedStackTraces()
             // .Enrich.WithExceptionDetails( new DestructingOptionsBuilder().WithIgnoreStackTraceAndTargetSiteExceptionFilter() )
             .MinimumLevel.ControlledBy( serilogLevelSwitch )
             // .MinimumLevel.Verbose()
             .CreateLogger();

        // enable self log to log internal Serilog exceptions

        Serilog.Debugging.SelfLog.Enable( Console.Error );
        Serilog.Debugging.SelfLog.Enable( message => System.Diagnostics.Debug.WriteLine( message ) );

        // Add access to generic IConfigurationRoot
        _ = serviceCollection.AddSingleton( configuration );

        return serviceCollection;
    }

    /// <summary>
    /// Create a service collection and configure and add Serilog services to this collection.
    /// </summary>
    /// <remarks>   2024-07-24. </remarks>
    /// <exception cref="FileNotFoundException">    Thrown when the requested file is not present. </exception>
    /// <param name="settingsFileName">     Filename of the settings file. </param>
    /// <param name="serilogLevelSwitch">   The Serilog level switch. </param>
    /// <param name="traceSourceSwitch">    The trace source switch. </param>
    /// <param name="providerKinds">        The provider kinds; defaults to <see cref="LoggingProviderKinds.Serilog"/>. </param>
    /// <returns>   An IServiceCollection. </returns>
    public static IServiceCollection ConfigureLoggingServices( string settingsFileName,
        Serilog.Core.LoggingLevelSwitch serilogLevelSwitch,
        System.Diagnostics.SourceSwitch traceSourceSwitch,
        LoggingProviderKinds providerKinds = LoggingProviderKinds.Serilog )
    {
        return Orlogger.ConfigureLoggingServices( new ServiceCollection(), settingsFileName, serilogLevelSwitch, traceSourceSwitch, providerKinds );
    }

    #endregion

    #region " Logging providers "

    /// <summary>   Gets or sets the logging services provider. </summary>
    /// <value> The service provider. </value>
    public ServiceProvider? LoggingServicesProvider { get; set; }

    /// <summary>   Builds service provider. </summary>
    /// <remarks>   2024-07-25. </remarks>
    /// <param name="serviceCollection">    Specifies the contract for a Collection of service
    ///                                     descriptors. </param>
    /// <returns>   A ServiceProvider? </returns>
    public ServiceProvider? BuildServiceProvider( IServiceCollection serviceCollection )
    {
        this.LoggingServicesProvider = serviceCollection.BuildServiceProvider();
        return this.LoggingServicesProvider;
    }

    /// <summary>   Gets the logger. </summary>
    /// <value> The logger. </value>
    public static Serilog.ILogger Logger => Serilog.Log.Logger;

    /// <summary>   Reset <see cref="Log.Logger"/> to default and dispose the original if possible.. </summary>
    /// <remarks>   David, 2021-03-13. </remarks>
    public static void CloseAndFlush()
    {
        Serilog.Log.CloseAndFlush();
    }

    /// <summary>   Creates a Serilog logger. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="settingsFileName"> Filename of the settings file. </param>
    /// <returns>   A new <see cref="Microsoft.Extensions.Logging.ILogger{TCategory}"/> logger using the Serilog provider. </returns>
    public Microsoft.Extensions.Logging.ILogger<TCategory>? CreateSerilogLogger<TCategory>( string settingsFileName )
    {
        this.LogSettingsFileName = settingsFileName;
        this.LoggingServicesProvider = Orlogger.ConfigureLoggingServices( settingsFileName,
            this.SerilogLevelSwitch, this.TraceSourceSwitch, LoggingProviderKinds.Serilog ).BuildServiceProvider();
        return this.LoggingServicesProvider?.GetService<ILoggerFactory>()?.CreateLogger<TCategory>();
    }

    /// <summary>   Creates a Serilog logger. </summary>
    /// <remarks>   2024-07-29. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="assembly"> The assembly, which could be the calling, executing or entry
    ///                         assembly. </param>
    /// <returns>   The new Serilog logger. </returns>
    public ILogger<TCategory>? CreateSerilogLogger<TCategory>( Assembly assembly )
    {
        return this.CreateSerilogLogger<TCategory>( Orlogger.BuildAppSettingsFileName( assembly ) );
    }

    /// <summary>   Creates a Serilog logger. </summary>
    /// <remarks>   2024-07-29. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="callingAssemblyMemberType">    Type of the calling assembly member. </param>
    /// <returns>   The new Serilog logger. </returns>
    public ILogger<TCategory>? CreateSerilogLogger<TCategory>( Type callingAssemblyMemberType )
    {
        return this.CreateSerilogLogger<TCategory>( Orlogger.BuildAssemblySettingsFileName( callingAssemblyMemberType ) );
    }

    /// <summary>   Creates the logger. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <returns>   The new logger. </returns>
    public ILogger<TCategory>? CreateLogger<TCategory>()
    {
        return this.LoggingServicesProvider?.GetService<ILoggerFactory>()?.CreateLogger<TCategory>();
    }

    /// <summary>   Creates the logger. </summary>
    /// <remarks>   David, 2021-02-22. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <param name="settingsFileName"> Filename of the settings file. </param>
    /// <param name="providerKinds">    The provider kinds. </param>
    /// <returns>   The new logger. </returns>
    internal ILogger<TCategory>? CreateLogger<TCategory>( string settingsFileName, LoggingProviderKinds providerKinds )
    {
        this.LogSettingsFileName = settingsFileName;
        _ = Orlogger.ConfigureLoggingServices( settingsFileName, this.SerilogLevelSwitch, this.TraceSourceSwitch, providerKinds );
        return this.LoggingServicesProvider?.GetService<ILoggerFactory>()?.CreateLogger<TCategory>();
    }

    /// <summary>   Gets the logger. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <typeparam name="TCategory">    Type of the category. </typeparam>
    /// <returns>   The logger. </returns>
    internal ILogger<TCategory>? GetLogger<TCategory>()
    {
        return this.LoggingServicesProvider?.GetService<ILogger<TCategory>>();
    }

    #endregion

    #region " logger settings file name "

    /// <summary>
    /// Gets or sets the filename of the log settings file, which defaults to the Entry assembly name.
    /// </summary>
    /// <value> The filename of the log settings file. </value>
    public string? LogSettingsFileName { get; private set; }

    #endregion

}
