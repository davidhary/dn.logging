namespace cc.isr.Logging.Orlog;

/// <summary>   A bit-field of flags for specifying the kinds of logging provider. </summary>
/// <remarks>   David, 2021-02-08. </remarks>
[Flags]
public enum LoggingProviderKinds
{
    /// <summary>   A binary constant representing the none flag. </summary>
    None = 0,
    /// <summary>   A binary constant representing the console log provider. </summary>
    ConsoleLog = 1,
    /// <summary>   A binary constant representing the debug log provider. </summary>
    DebugLog = 2,
    /// <summary>   A binary constant representing the trace log provider </summary>
    TraceLog = 4,
    /// <summary>   A binary constant representing the Serilog provider. </summary>
    Serilog = 8
}


