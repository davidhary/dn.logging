# Open Source

Support libraries extending the Visual Studio framework for engineering and measurements.

* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Open-Source"></a>
## Open Source
Open source used by this software is described and licensed at the
following sites:  
[logging]  
[tracing]  
[json]  
[Exception Extension]  
[Serilog]  
[Serilog.Enrichers.Demystify]  
[Serilog.Sinks.Async]  
[Serilog.Sinks.File]  
[Serilog.Sinks.File.Header]  

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
 
[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[logging]: https://www.bitbucket.org/davidhary/dn.logging
[tracing]: https://www.bitbucket.org/davidhary/dn.tracing
[json]: https://www.bitbucket.org/davidhary/dn.json
[Exception Extension]: https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
[Serilog]: https://github.com/serilog/serilog
[Serilog.Enrichers.Demystify]: https://github.com/nblumhardt/serilog-enrichers-demystify
[Serilog.Sinks.Async]: https://github.com/serilog/serilog-sinks-async 
[Serilog.Sinks.File]: https://github.com/serilog/serilog-sinks-file
[Serilog.Sinks.File.Header]: https://github.com/cocowalla/serilog-sinks-file-header

[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[external repositories]: ExternalReposCommits.csv

